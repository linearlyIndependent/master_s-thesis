\section{Motivation}

Container technologies, containerization, container orchestrators and Kubernetes are nowadays not only widely researched topics in the scientific community, but also significant driving forces in core technological industries. More and more organizations are running parts of their operations on container technologies and are swiftly expanding into these areas. Professionals with expertise in these areas are in high demand on the labor market.

As part of its quest to stay at the \gls{soa} of information technology, the Faculty of Computer Science of the \gls{uas} Technikum Wien is constantly looking into ways to expand its knowledge of Kubernetes based container platforms, microservice architectures and container technologies. This thesis, as well as the corresponding project, are manifestations of this desire, with the goal to enrich the faculty both with theoretical and operational knowledge. 

There is a constant demand at the faculty for the provisioning of a wide range of services for different courses, research projects and the faculty infrastructure. Although some of these services have long or indefinite lifespans, most of the them only last for the given project or course and are decommissioned afterwards. Multiple different hypervisors and web-hosting platforms are in use to provision these services with the corresponding infrastructure, with a faculty Kubernetes cluster having been in consideration since a long time. To further optimize the service provisioning and to embrace the \gls{soa} in container orchestration, a faculty Kubernetes cluster is going to be created based on the findings of this thesis. Before the cluster can be created, the most suitable Kubernetes distribution needs to be found and an overview of the current Kubernetes product landscape needs to be determined.

Due to the great popularity of Kubernetes, major cloud providers as well as many companies currently active in the virtualization and containerization field provide their own Kubernetes distributions (Kubernetes bundled with other software, adapted, enhanced and specialized for different needs). Choosing a Kubernetes distribution, which fulfills all requirements and provides a reasonable value for money is an exceptionally difficult task, requiring the evaluation of potentially hundreds of products from different companies. This thesis aims to fulfill this task and assist stakeholders by providing both an introduction into core Kubernetes concepts and components as well as to provide a decision aid for choosing the right Kubernetes distribution.


\section{Related Studies, the Knowledge Gap and the Expected Contributions}
\subsection{Related Studies}
A wide range of papers and studies are available when considering the area of Kubernetes, container runtimes and container orchestrators. Related work, which is addressed and discussed as part of this thesis includes the following:

A detailed insight into the market trends and usage tendencies of container orchestrators is provided by a wide range of market reports \cite{bartolettiForresterNewWave2018a,daiForresterWaveMulticloud2020} and user surveys \cite{2019ContainerAdoption2019,StateContainerKubernetes2020,CNCFSurvey20182018,CNCFSurvey20192019,StateKubernetes20202020,CNCFSurvey20202020}, which are addressed in \autoref{subsec:distribution_trends}. Reports and user surveys also provide an insight into different aspects of Kubernetes adoption. The motivators and benefits of Kubernetes adoption are provided by \cite{CNCFSurvey20182018,2019ContainerAdoption2019,StateKubernetes20202020,CNCFSurvey20192019,CNCFSurvey20202020} and are handled in \autoref{subsec:motivators_benefits_adoption}. Which issues, challenges and security implications an adoption of Kubernetes poses are covered by \cite{2019ContainerAdoption2019,CNCFSurvey20182018,CNCFSurvey20192019,StateContainerKubernetes2020,Sysdig2021Container2021,jianDefenseMethodDocker2017,casalicchioStateArtContainer2020} and are summarized in \autoref{subsec:issues_challenges_adoption}. An insight into Kubernetes usage patterns and into the size of a typical Kubernetes cluster is provided by \cite{Sysdig2021Container2021,CNCFSurvey20202020,CNCFSurvey20192019,ShareClusterNamespaces} and is a subject of \autoref{subsec:kubernetes_usage_patterns}.

Considering container runtimes, insights into the market shares of different container runtimes are provided by reports \cite{StateContainerKubernetes2020,Sysdig2021Container2021} and performance comparisons between different runtimes are provided by \cite{martinExploringSupportHigh2018,kovacsComparisonDifferentLinux2017,kozhirbayevPerformanceComparisonContainerbased2017}. Both topics are addressed in \autoref{subsec:intro_runtimes_performance_trends}. Since Kubernetes clusters are often used together with different development tools, reports and surveys \cite{CNCFSurvey20182018,CNCFSurvey20192019,CNCFSurvey20202020,Sysdig2021Container2021} also cover this aspect of Kubernetes usage, which is summarized by \autoref{subsec:devops_tools_trends}.

Related work, which is not addressed in further chapters of this thesis are different performance benchmarks between Kubernetes and other container orchestrators \cite{PerformanceComparisonContainer2020,razaEmpiricalPerformanceEnergy2021,panPerformanceComparisonCloudBased2019a,perezPerformanceComparisonVirtualization2021,truyenComprehensiveFeatureComparison2019}.


\subsection{The Knowledge Gap}
\label{subsec:introduction_knowledge_gap}

The comparison of different Kubernetes distributions has only been conducted in a small number of specific areas. These comparisons were mostly performance focused, such as studies comparing minimalist Kubernetes distributions in resource constrained environments \cite{fogliPerformanceEvaluationKubernetes2021,bohmProfilingLightweightContainer}, studies comparing different cloud-only Kubernetes distributions \cite{pereiraferreiraPerformanceEvaluationContainers2019b,truyenManagingFeatureCompatibility2020a} or a study comparing standard Kubernetes and OpenShift \cite{prashanthjakkulaContainerRuntimePerformance2019}.

Two papers could be found with similar objectives and contents as this paper, the first being the thesis of Juntunen \cite{juntunenOpenShiftEnterpriseFleet2020}. The thesis compares different \glspl{vim}, namely OpenShift, SUSE CAAS, OpenStack and VMWare vSphere across a wide range of aspects to determine the most suitable option for a given project. In this aspect, it has a similar aim as this thesis. Furthermore, the author evaluates the selected \glspl{vim} both using theoretical comparison (based on the documentation) as well as practical usecases on real infrastructure, which is identical to the two-level evaluation planned as part of this thesis. Furthermore, it also uses some of the same evaluation criteria as this thesis. However, the mentioned thesis does not only analyze Kubernetes distributions and analyzes the \glspl{vim} with a very different focus. The thesis focuses on enterprise fleet management and the feature scope, language bindings and monitoring capabilities provided by the product's \gls{api}. This thesis however focuses exclusively on Kubernetes distributions and evaluates them based on a wide range of system-administration, maintainability and usability criteria as well as practical usecases. 

The second similar paper \cite{truyenComprehensiveFeatureComparison2019} provides a thorough analysis and comparison of different container orchestrators, namely Kubernetes, Docker Swarm and multiple Mesos-based solutions. It defines a detailed theoretical framework for evaluating different container orchestrators and identifies 178 features along which container orchestrators can be compared. Most of the features defined by that paper are either much more granular or much more generic than the aspects used by this thesis. Furthermore, many criteria defined in that paper are specific for the comparison of different container orchestrators and are irrelevant for comparing only Kubernetes distributions, since they apply equally for all of them. In further parts of the paper, the theoretical framework is expanded and performance comparison between the different orchestrators is provided. Although that paper conducts a detailed feature comparison of the different orchestrators, it does not provide any practical evaluation since all findings are based either on other sources or theoretical evaluation. 


\subsection{Expected Contributions}

This thesis aims to fill the following knowledge gaps:
\begin{itemize}
	\item There has been no paper providing a summary of all currently available Kubernetes-compatible container runtimes and their relationships.
	\item There have been only two studies \cite{truyenComprehensiveFeatureComparison2019,juntunenOpenShiftEnterpriseFleet2020} to compare different container orchestrators based on non-performance-focused criteria.
	\item There has been no study that compares Kubernetes distributions based on non-performance-focused criteria.
	\item There has been no study that compares Kubernetes distributions which can be self-provisioned on local infrastructure and are either free to use or offer community version with similar feature scope.
	\item There has been no resource (including official Red Hat resources) to provide a detailed and practical comparison between \gls{ocp} and its community version \gls{okd}.
	\item There has been no study that compares different Kubernetes distributions based on system-administration, maintainability and usability criteria.
	\item There has been no study that compares the usability of different Kubernetes distributions based on multiple real-world projects which are characteristic for a university's computer science faculty.
\end{itemize}

\section{Research Questions}

\begin{itemize}
	\item Which container runtimes can be used in a Kubernetes cluster, how do these runtimes connect to each other and which standards are used to enforce interoperability?
	\item Which \gls{cncf} certified Kubernetes distributions satisfy the project's base requirements (see \autoref{sec:comparison_matrix_methods}) and how do they compare to each other when evaluated with a feature matrix?
	\item How do the most promising Kubernetes distributions compare to each other when deployed on similar test architectures?
	\begin{itemize}
		\item How do the Kubernetes distributions perform during a wide range of system-administration focused maintenance tasks?
		\item How do the Kubernetes distributions compare when considering end-user focused usability criteria?
		\item How do the Kubernetes distributions perform during real-world projects which are characteristic for a university's computer science faculty?
	\end{itemize}
\end{itemize}

%\begin{itemize}
%	\item Given the use-cases and requirements, how do the analyzed \acrshort{soa} Kubernetes distributions compare to each other?
%	\begin{itemize}
%		\item Given the use-cases and requirements, which Kubernetes distribution is the most suitable?
%	\end{itemize}
%	\item How can an initial version of the required architecture be developed with the chosen
%	Kubernetes distribution?
%	\item How does the selected subset of Kubernetes distributions compare when deployed in identical reference architectures.
%	\item Given the department requirements and the Kubernetes distribution found most suitable, how can the Kubernetes cluster be deployed?
%	\begin{itemize}
%		\item What steps are necessary to integrate the cluster into the department infrastructure?
%		\item If there are requirements not supported by the Kubernetes distribution, how can those requirements be satisfied with additional tools?
%		\item What advantages or disadvantages can be observed and how can the existing architecture be improved?
%	\end{itemize}
%\end{itemize}

\section{Limitations}
\begin{itemize}
	\item Due to financial limitations, Kubernetes distributions with no free-to-use version cannot be analyzed.
	\item Only five or less Kubernetes distributions can be thoroughly analyzed due to practical considerations and timely constraints.
	\item Due to limitations in the available computational resources, the nodes of each cluster will have the manufacturer recommended amount of \gls{cpu} cores, \gls{ram} and other resources, not more.
\end{itemize}


\section{Overview of the Thesis Structure}
The rest of the thesis is organized as follows:

The most-relevant organizations, standards, certifications and the main Kubernetes components are introduced in \autoref{ch:intro}. This chapter also introduces the main Kubernetes objects and gives an overview of the authentication, authorization and quota management features of Kubernetes. Lastly, it provides a complete overview of all Kubernetes-compatible container runtimes and summarizes the main Kubernetes usage trends.

A small subset of all \gls{cncf} certified Kubernetes distributions is analyzed in a feature matrix in \autoref{ch:comparison_matrix}. The Kubernetes distributions found to be the most promising are then analyzed in similar test architectures in \autoref{ch:comparison_cluster}. The findings of \autoref{ch:intro}, \autoref{ch:comparison_matrix} and \autoref{ch:comparison_cluster} are discussed in \autoref{ch:discussion}, while the conclusions are provided in \autoref{ch:conclusions}. Lastly, the appendices provide a multitude of code snippets (\autoref{ch:code_snippets}) and tables summarizing the findings of different chapters.
