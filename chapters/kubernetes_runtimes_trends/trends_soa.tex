\section{Containers and Kubernetes Trends and State Of the Art}
\label{sec:trends_soa}
The field of container technologies experiences constant development as new technologies emerge and well-established technologies are acquired, merged into other projects or get discontinued. In such an environment, market research reports provide insight into the major developments of the market and the possible directions into which the field of container technologies might advance into. Furthermore, user survey reports provide insight into the motivation, aims and issues of companies which are investing in using container technologies in their production environments. 

In the following sections, the \gls{soa} and trends in container technologies are determined, starting with 2018 up to 2021.

Based on the following findings, a ``Kubernetes Readiness Factsheet and Checklist'' is available in \autoref{ch:appendix_readiness_factsheet_checklist}, which can help organizations to evaluate their readiness for adopting Kubernetes. 

\subsection{Motivators and Benefits of Adoption}
\label{subsec:motivators_benefits_adoption}
As the main motivators for adapting container technologies and cloud native tools, the \gls{cncf}’s 2018 Survey \cite{CNCFSurvey20182018} found the need for faster deployments, the possibility for improved scalability and the portability between different cloud vendors. PortWorx and Aqua Security’s 2019 Container Adoption Survey \cite{2019ContainerAdoption2019} also found the aim to avoid vendor lock in as one of the main motivators. Other motivators found by the survey include decreased infrastructure costs and increased development speed, efficiency and agility. Lastly, VMWare’s 2020 State of Kubernetes \cite{StateKubernetes20202020} study lists, apart from the already noted faster development and reduced costs, the improved resource utilization, the containerization of legacy monoliths and the ability to move services to the cloud as motivators.

Companies experience a wide range of benefits when using cloud-native tools. In the \gls{cncf}’s 2019 \cite{CNCFSurvey20192019} and 2020 \cite{CNCFSurvey20202020} survey, a fulfillment of the previously described motivators can be observed, since most of them are reported as experienced benefits by the respondents. Both surveys note the improved scalability, faster deployments, decreased costs and increased developer productivity, while the 2019 survey also notes cloud portability as an experienced benefit. Finally, additionally to the fulfilled motivators, both surveys note the improved availability of applications and infrastructure as a further experienced benefit.

\subsection{Issues and Challenges of Adoption}
\label{subsec:issues_challenges_adoption}
Although the previous findings suggest a wide range of experienced benefits and the fulfillment of motivators, companies are facing a multitude of issues when adopting cloud-native tools.

The PortWorx - Aqua Security 2019 Container Adoption Survey \cite{2019ContainerAdoption2019} and the \gls{cncf}’s 2018 \cite{CNCFSurvey20182018} and 2019 \cite{CNCFSurvey20192019} survey all highlight the respondents’ issues with the security, networking, monitoring and scalability aspects of their cloud-native infrastructure. These findings can be partially attributed to the fact, that cloud native infrastructures, such as a Kubernetes cluster, come with a large setup and maintenance complexity and require a significant amount of existing knowledge in the area. Both the 2018 and the 2019 \gls{cncf} survey supports this notion by highlighting that the faced complexity of such infrastructures and the lack of training are two of the top three challenges faced by respondents. 
Additionally to the previous findings, the PortWorx - Aqua Security survey also signalizes data management as the second most common challenge for respondents, and related issues such as data security concerns, data loss concerns, incompatibility of legacy storage technologies and inadequate storage management tools to be in the top ten concerns of respondents.

\subsubsection{Container and Cluster Security Issues}
Container and cluster security issues are also amongst the top three challenges faced by respondents of all analyzed surveys and reports. According to the StackRox 2020 State of Container and Kubernetes Security Report \cite{StateContainerKubernetes2020}, 90\% of respondents experienced at least one security incident in their Kubernetes and container environments during the past year. Out of these 90\%, 67\% could be attributed to misconfiguration, 22\% to a major vulnerability, 17\% to a runtime incident and 16\% were failed security audits. Based on these findings and the inherent complexity of Kubernetes and other cloud-native infrastructures, the report found the case of a major vulnerability which can be exploited due to a misconfiguration to be the most likely attack scenario on such infrastructures. For a visualization of these findings, refer to \autoref{fig:incident_statistics_kubernetes}.

Apart from the data management concerns analyzed in the previous section and the general threats listed in the previous paragraph, container image vulnerabilities were found to be considerable threats by the PortWorx - Aqua Security survey and the Sysdig 2021 Container Security and Usage Report \cite{Sysdig2021Container2021}. According to both studies, the runtime protection and monitoring of containers, the vulnerability management of container images and security automation as part of the \gls{cicd} pipeline are amongst the most frequent security issues. This is backed by multiple findings of the Sysdig report, which are based on customer surveys and the analysis of aggregated image scan and runtime monitoring logs of its container security platform. Firstly, 55\% of scanned container images had high severity or more serious vulnerabilities, from which 4\% of \gls{os} vulnerabilities and 53\% of non-\gls{os} vulnerabilities were critical. Secondly, 58\% of analyzed container images were configured to be run as root. Lastly, other frequently observed security violations included the mounting of sensitive volumes and directories of the host into containers, especially the mounting of directories such as /, /root, /etc as read-write, and the specification of shells with attached terminals as container entry points. A visualization of these findings is provided in \autoref{fig:incident_statistics_containers}.

It has to be noted, that security issues are not only caused by vulnerabilities, runtime violations or misconfigurations, but also by conscious management decisions. According to the StackRox report \cite{StateContainerKubernetes2020}, 34\% of respondents think that their companies are not investing sufficient resources into container security and 15\% of respondents believe their management is not taking container security threats seriously. Furthermore, according to the Sysdig report \cite{Sysdig2021Container2021}, 74\% of respondents scan container images as part of their \gls{cicd} pipeline. This by itself might be a great percentage, but it also means that 26\% of Sysdig customers, who should have access to Sysdig’s image scanning tool, are not using this available security feature.


\begin{figure}
	\centering
	\begin{subfigure}{.475\linewidth}
		\centering
		\begin{tikzpicture}
			\pgfplotsset{
				every axis legend/.append style={
					at={(0.67,0.8)},
					anchor=south
				},
			}
			
			\begin{axis}[
				axis lines = left,
				x axis line style = -,
				y axis line style = {-latex},
				legend style={font=\tiny},
				%legend pos=outer north east,legend
				%style={at={(40.5,-0.1)},anchor=north},
				%xlabel = $n_f$,
				scale only axis,
				%width=\linewidth,
				xlabel = ~,
				%axis x line=none,
				ylabel = {Respondents affected (\%)},
				ymajorgrids=true,
				grid style=dashed,
				width=0.8\linewidth,
				xtick=\empty,
				every axis plot/.append style={ultra thick},
				%major x tick style = transparent,
				%enlarge x limits=0.25,
				%axis x line=none,
				xmin=0,
				xmax=6,
				ymin=0,
				ymax=100,
				]
				\addplot+ [
				ybar, red, fill, mark=*, mark options={scale=2, black, fill=white},% mark=none,
				] coordinates {
					(1,90)
				};
				\addlegendentry{Any Kubernetes/container incident: $90\%$}
				
				\addplot+ [
				ybar, green, fill, mark=square*, mark options={scale=2,black, fill=white},% mark=none,
				] coordinates {
					(2,67)
				};
				\addlegendentry{Misconfiguration: $67\%$}
				
				
				\addplot+ [
				ybar, blue, fill, mark=diamond*, mark options={scale=2, black, fill=white},% mark=none,
				] coordinates {
					(3,22)
				};
				\addlegendentry{Major vulnerability: $22\%$}
				
				\addplot+ [
				ybar, orange, fill, mark=triangle*, mark options={scale=2, black, fill=white},% mark=none,
				] coordinates {
					(4,17)
				};
				\addlegendentry{Runtime incident: $17\%$}
				
				\addplot+ [
				ybar, lightgray, fill, mark=otimes*, mark options={scale=2, black, fill=white},% mark=none,
				] coordinates {
					(5,16)
				};
				\addlegendentry{Failed audit: $16\%$}
			\end{axis}
		\end{tikzpicture}
		\caption{Kubernetes and container related security incidents experienced at least once a year by respondents \cite{StateContainerKubernetes2020}.}
		\label{fig:incident_statistics_kubernetes}
	\end{subfigure}%
	\hfill%
		\begin{subfigure}{.475\linewidth}
		\centering
		\begin{tikzpicture}
			\pgfplotsset{
				every axis legend/.append style={
					at={(0.5,1.02)},
					anchor=south
				},
			}
			
			\begin{axis}[
				axis lines = left,
				x axis line style = -,
				y axis line style = {-latex},
				legend style={font=\tiny},
				%legend pos=outer north east,legend
				%style={at={(40.5,-0.1)},anchor=north},
				%xlabel = $n_f$,
				scale only axis,
				every axis plot/.append style={ultra thick},
				%width=\linewidth,
				xlabel = ~,
				%axis x line=none,
				ylabel = {Scanned containers (\%)},
				ymajorgrids=true,
				grid style=dashed,
				width=0.8\linewidth,
				xtick=\empty,
				ytick distance=10,
				%major x tick style = transparent,
				%enlarge x limits=0.25,
				%axis x line=none,
				xmin=0,
				xmax=5,
				ymin=0,
				ymax=60,
				]
				\addplot+ [
				ybar, red, fill, mark=*, mark options={scale=2, black, fill=white},% mark=none,
				] coordinates {
					(1,55)
				};
				\addlegendentry{Containers failing security scan: $55\%$}
				
				\addplot+ [
				ybar, green, fill, mark=square*, mark options={scale=2, black, fill=white},% mark=none,
				] coordinates {
					(2,58)
				};
				\addlegendentry{Containers running as root: $58\%$}
				
				\addplot+ [
				ybar, blue, fill, mark=diamond*, mark options={scale=2, black, fill=white},% mark=none,
				] coordinates {
					(3,53)
				};
				\addlegendentry{Non-\acrshort{os} vulnerabilities being high or critical: $53\%$}
				
				\addplot+ [
				ybar, orange, fill, mark=triangle*, mark options={scale=2, black, fill=white},% mark=none,
				] coordinates {
					(4,4)
				};
				\addlegendentry{\acrshort{os} vulnerabilities being high or critical: $4\%$}
				
			\end{axis}
		\end{tikzpicture}
		\caption{Aggregated container scan statistics of Sysdig customers \cite{Sysdig2021Container2021}.}
		\label{fig:incident_statistics_containers}
	\end{subfigure}%

	\caption[Statistics of Kubernetes and container security incidents and vulnerabilities.]{Statistics of Kubernetes and container security incidents and vulnerabilities based on \cite{StateContainerKubernetes2020,Sysdig2021Container2021}.}
	\label{fig:incident_vulnerabilities_statistics}
\end{figure}

\subsection{Container Orchestrators and Kubernetes Distributions Trends}
\label{subsec:distribution_trends}
The following analysis looks at two aspects of the container technology market in the years 2018 to 2021. Firstly, it looks at the changes in market share between both hosted and on-premise Kubernetes distributions, and the market share of different container orchestrators, with a visualization being available in  \autoref{fig:orchestrators_market_share} and the raw data being available in \autoref{tab:distributions_trends_data}. Secondly, it looks at container technology and Kubernetes usage patterns and their development. Since most studies do not survey Kubernetes distributions separately from container orchestrators, they can only be analyzed together.

The Forrester Enterprise Container Platform Software Suites, Q4 2018 report \cite{bartolettiForresterNewWave2018a} highlighted Red Hat, Rancher Labs and Docker as market leaders in the development of container platforms. Red Hat was found to provide outstanding overall user experience and strong integration of container technologies with its other products and ecosystem. Rancher excelled due to its DevOps-focused products and enterprise level VMware integration. Docker was found to still hold strong positions not only due to its wide acceptance based on its legacy in the history of container technologies, but also based on its robustness and superior container supply chain. The report also found VMWare with its Tanzu product line and strong market presence to be a strong contestant. Through the acquisitions of Bitnami, Heptio and Pivotal and the great integration of their products into its existing virtualization technology it could strengthen its market presence with the offering of highly integrated Kubernetes products to its existing customers.

The market share of container orchestrators not analyzed by the 2018 Forrester report can be be found in the \gls{cncf}’s 2018 Survey \cite{CNCFSurvey20182018}. Amazon \gls{ecs}, which is not based on Kubernetes, was found to be most widely used container service, used by 63\% of respondents. Different on-premise solutions were found to be the second most widely used option with 43\%, followed by the \gls{gcp} which was used by 35\% of respondents. While Amazon, Google and different on-premise solutions were all found to have lost market share compared to 2017, Microsoft Azure increased its market share and was found to be used by 29\% of respondents. VMWare’s solutions were found to be used by 24\% of respondents, which is in line with the findings of the 2018 Forrester report of it being a strong contestant.

In 2019, the takeover of different Kubernetes distributions can be observed on the container orchestrator market. Compared to 2018, the general power relations did not shift considerably between different corporations, but their Kubernetes based orchestrators overtook their non-Kubernetes based orchestrators in all analyzed surveys. According to the PortWorx - Aqua Security 2019 Container Adoption Survey \cite{2019ContainerAdoption2019}, contrary to the \gls{cncf}’s 2018 survey, Microsoft Azure was found to be the most widely used container orchestrator, followed by Google and Amazon. When only considering Kubernetes distributions, Microsoft \gls{aks} was found to be the most widely used Kubernetes Runtime, used by 47\% of respondents. \gls{aks} was followed by \gls{iks} with 40\%, and \gls{gke} and Amazon \gls{eks} with both over 30\%. Red Hat's \gls{ocp} was found to be used by 23\% of respondents, while Docker Swarm and standard Kubernetes were used by 11\% of respondents.

The \gls{cncf}’s 2019 survey found Amazon \gls{eks} to be the most widely used container orchestrator, closely followed by \gls{gke}, which was followed by Docker, \gls{aks}, standard Kubernetes and \gls{ocp}. These results differ significantly from the PortWorx – Aqua Security survey in the measured usage of \gls{aks}, but for the other orchestrators there are only differences of one or two positions. Based on both surveys, \gls{eks}, \gls{gke} and \gls{aks} were found to be the most used container orchestrators in 2019. \gls{iks}, \gls{ocp}, Docker (Swarm, \gls{ee}, \gls{ce}) and standard Kubernetes can be seen as strong contestant, while it has to be noted that a weakening of Docker’s market position can be observed despite its acquisition by Mirantis. The usage of Rancher was not measured in the studies analyzed from 2019.

The container technology market experienced some major changes in 2020, analyzed by the Forrester Multicloud Container Development Platforms Q3 2020 report \cite{daiForresterWaveMulticloud2020}. IBM successfully acquired Red Hat and started merging the key solutions of the two companies into an ecosystem and product palette that made Red Hat and IBM the market leader by 2020. Red Hat \gls{ocp} also became the most deployed multi-cloud container platform. Rancher Labs, which was acquired by SUSE \cite{SUSECompletesAcquisition2020}, kept its great market position, while Docker lost its outstanding market position. In contrast to the IBM – Red Hat acquisition, Docker’s acquisition by Mirantis did not strengthen its market position while market interest started to shift to other vendors and technologies providing stronger technological innovation.

The StackRox 2020 State of Container and Kubernetes Security Report \cite{StateContainerKubernetes2020} gives a more detailed insight into the power relations of the container technology market in 2020. Interestingly, self-managed standard Kubernetes was found to be the most widely used container orchestrator, used by 50\% of respondents. Standard Kubernetes was followed by \gls{eks} with 44\%, \gls{aks} with 31\% and \gls{ocp} with 22\%. \gls{gke}, Docker (Swarm, \gls{ee}), Rancher and \gls{iks} were all below the 20\% margin. Although all other orchestrators in the 5-20\% segment experienced a decrease in popularity, Rancher experienced a 36\% usage growth compared to 2019. Based on both surveys, \gls{eks}, \gls{gke}, \gls{ocp} and \gls{aks} were found to be the most used container orchestrators also in 2020. \gls{gke} scored lower and \gls{ocp} scored higher compared to the 2019 surveys. Standard Kubernetes and Rancher can be seen as strong contestants, while Docker (Swarm, \gls{ee}, \gls{ce}) continues to loose market share.


\begin{figure}[h!]
	\centering
	\begin{tikzpicture}
		\pgfplotsset{
			every axis legend/.append style={
				%				at={(0.5,1.03)},
				at={(1.2,0.1)},
				anchor=south
			},
		}
		
		\begin{axis}[
			width=0.78\linewidth,
			height=0.5\linewidth,
			axis lines = left,
			%legend style={font=\bfseries},
			%legend style={font=\small},
			legend style={font=\scriptsize},
			%legend pos=outer north east,legend
			%style={at={(40.5,-0.1)},anchor=north},
			%scale only axis=true,
			%width=\linewidth,
			xlabel = {Time (years)},
			ylabel = {Market share (\%)},
			ymajorgrids=true,
			grid style=dashed,
			date coordinates in=x,
			xtick = {2018-01-01,2019-01-01,2020-01-01},
			xticklabel = \year,
			ytick distance=10,
			date ZERO=2018-01-01,
			xmin = 2018-01-01,
			xmax = 2020-01-01,
			%ylabel = {execution time (s)},
			every axis plot/.append style={ultra thick},
			cycle list name=color list]
			
			\addplot [
			color=red,
			mark=triangle*,
			mark options={scale=2,fill=red},
			]
			coordinates {(2018-01-01,63)(2019-01-01,33)(2020-01-01,20)};
			\addlegendentry{\acrshort{ecs}* / \acrshort{ec2}*}
			
			\addplot [
			color=green,
			mark=|,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,nan)(2019-01-01,17)(2020-01-01,22)};
			\addlegendentry{\acrshort{ocp}}
			
			\addplot [
			color=blue,
			mark=star,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,nan)(2019-01-01,40)(2020-01-01,4)};
			\addlegendentry{\acrshort{iks}}
			
			\addplot [
			color=lightgray,
			mark=oplus*,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,nan)(2019-01-01,33)(2020-01-01,19)};
			\addlegendentry{\acrshort{gke}}
			
			\addplot [
			color=pink,
			mark=triangle*,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,nan)(2019-01-01,32)(2020-01-01,44)};
			\addlegendentry{\acrshort{eks}}
			
			\addplot [
			color=black,
			mark=x,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,nan)(2019-01-01,11)(2020-01-01,50)};
			\addlegendentry{Standard Kubernetes}
			
			\addplot [
			color=brown,
			mark=*,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,9)(2019-01-01,7)(2020-01-01,3)};
			\addlegendentry{Mesosphere* / Mesos*}
			
			\addplot [
			color=olive,
			mark=diamond*,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,21)(2019-01-01,11)(2020-01-01,19)};
			\addlegendentry{Docker \acrshort{ee}* / Swarm*}
			
			\addplot [
			color=orange,
			mark=square*,
			mark options={scale=2,fill=orange},
			]
			coordinates {(2018-01-01,7)(2019-01-01,11)(2020-01-01,4)};
			\addlegendentry{\acrshort{pcf}*}
			
			\addplot [
			color=cyan,
			mark=square*,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,nan)(2019-01-01,8)(2020-01-01,4)};
			\addlegendentry{\acrshort{pks}}
			
			\addplot [
			color=lime,
			mark=pentagon*,
			mark options={scale=2,fill=white},
			]
			coordinates {(2018-01-01,29)(2019-01-01,34)(2020-01-01,31)};
			\addlegendentry{\acrshort{aks}}
			
		\end{axis}
	\end{tikzpicture}  
	
	\caption*{	
		% Table note table, bit hacky but it works.
		\begin{small}
		\begin{tabular}{c}
			%\toprule
			\textbf{*}: Partly or entirely non-Kubernetes-based container orchestrator. \\
			%\bottomrule
		\end{tabular}
		\end{small}
	}
	
	\caption[Market share of different container orchestrators between 2018 and 2020.]{Market share of different container orchestrators between 2018 and 2020, based on \cite{2019ContainerAdoption2019,StateContainerKubernetes2020,CNCFSurvey20182018,CNCFSurvey20192019}, with the raw data being available in \autoref{tab:distributions_trends_data}.}
	\label{fig:orchestrators_market_share}
\end{figure}


Considering the market share of all Kubernetes distributions againts other container orchestrators, a stable upward trend can be observed in favor of Kubernetes. In 2018, 58\% of respondents working with containers used Kubernetes in their production environment according to the \gls{cncf}’s 2018 survey \cite{CNCFSurvey20182018}, which increased to 78\% in the \gls{cncf}’s 2019 survey \cite{CNCFSurvey20192019}. In 2020, the share these respondents was measured between 59\% \cite{StateKubernetes20202020} and 83\% \cite{CNCFSurvey20202020}. The 75\% measured by the StackRox 2020 State of Container and Kubernetes Security Report \cite{StateContainerKubernetes2020} should therefore be a realistic value. 


\subsection{Kubernetes Usage Patterns}
\label{subsec:kubernetes_usage_patterns}
The surveys and reports analyzed between 2018 and 2020 also provide insight into the typical Kubernetes and container usage patterns of respondents. These surveys and reports were conducted on users of \gls{cncf} projects, on customers of container technology related services and other types of container technology users.

Report \cite{Sysdig2021Container2021} provides a detailed insight into their customers’ Kubernetes architectures. According to the findings of the report, their typical customer operates a single Kubernetes cluster (60\%), with 1-5 nodes (60\%). Considering production clusters only, the 2019 \cite{CNCFSurvey20192019} and 2020 \cite{CNCFSurvey20202020} \gls{cncf} surveys found a typical Kubernetes production environment to be consisting of 2-5 clusters (around 40\% of respondents). Continuing with the findings of report \cite{Sysdig2021Container2021}, the typical Kubernetes cluster has 6-10 namespaces (57\%) (including the 3 initial namespaces \cite{ShareClusterNamespaces}), and 2-4 Deployments per namespace (86\%). Furthermore, the typical Kubernetes cluster was found to have 51-100 Pods (38\%) and 16-25 Pods per node (43\% of respondents). The typical lifespan of a container was found to be under 5 minutes (49\%), while only 25\% of containers had a lifespan between 5 minutes and 1 hour. Containers running longer than one week made up only 17\% percent of all containers. Finally, the average image size observed was around 376 MB.

\subsection{Container Runtimes Performance and Trends}
\label{subsec:intro_runtimes_performance_trends}
The following section analyzes the performance and trends of high-level Kubernetes and non-Kubernetes container runtimes.

Although the usage of Kubernetes as a container orchestrator is a fixed requirement of this project, analyzing and comparing the performance of the major container technologies should still be beneficial even if some of them are not compatible with Kubernetes.

Docker was found to outperform \gls{lxc} in high performance computationally intensive applications with small problem sizes, in disk-I/O write performance and memory I/O performance \cite{martinExploringSupportHigh2018}. In terms of security, previous studies have found multiple container escape attack possibilities \cite{jianDefenseMethodDocker2017} and voiced concerns due to Docker’s lack of container layer encryption possibilities \cite{casalicchioStateArtContainer2020}. 

\gls{lxc} in comparison with Docker was found to perform nearly identical in CPU intensive \cite{kovacsComparisonDifferentLinux2017}, combined disk-I/O and network-I/O benchmarks \cite{kozhirbayevPerformanceComparisonContainerbased2017}. Furthermore \gls{lxc} was found to outperform Docker on overall memory performance (except I/O performance) and sequential file read performance \cite{kozhirbayevPerformanceComparisonContainerbased2017}.

On the usage of different high-level Kubernetes runtimes, only two studies are available. The StackRox 2020 State of Container and Kubernetes Security Report \cite{StateContainerKubernetes2020} found Docker to be the dominant high-level container runtime (used by 92\% of respondents), followed by containerd (17\% of respondents) and \acrshort{cri-o} (7\% of respondents). The Sysdig 2021 Container Security and Usage Report \cite{Sysdig2021Container2021} generally confirms these findings but also provides information about ongoing tendencies. Compared to its 2020 results, Sysdig measured a decrease of Docker usage from 79\% to only 50\%, an increase of containerd usage from 18\% to 33\% and an increase of \acrshort{cri-o} usage from 4\% to 17\%.

\begin{figure}[h!]
	\centering
	\begin{tikzpicture}
		\pgfplotsset{
			every axis legend/.append style={
				%				at={(0.5,1.03)},
				at={(1.4,0.3)},
				anchor=south
			},
		}
		
		\begin{axis}[
			width=0.6\linewidth,
			height=0.45\linewidth,
			axis lines = left,
			legend style={font=\scriptsize},
			xlabel = {Time (years)},
			ylabel = {Market share (\%)},
			ymajorgrids=true,
			grid style=dashed,
			date coordinates in=x,
			xtick = {2020-01-01,2021-01-01},
			xticklabel = \year,
			ytick distance=10,
			date ZERO=2020-01-01,
			xmin = 2020-01-01,
			xmax = 2021-01-01,
			every axis plot/.append style={ultra thick},
			cycle list name=color list]
			
			\addplot [
			color=red,
			mark=triangle*,
			mark options={scale=2,fill=red},
			]
			coordinates {(2020-01-01,85.5)(2021-01-01,50)};
			\addlegendentry{Docker: \textasciitilde{}85.5\% -> 50\%}
			
			\addplot [
			color=green,
			mark=square*,
			mark options={scale=2,fill=green},
			]
			coordinates {(2020-01-01,17.5)(2021-01-01,33)};
			\addlegendentry{containerd: \textasciitilde{}17.5\% -> 33\%}
			
			\addplot [
			color=blue,
			mark=oplus*,
			mark options={scale=2,fill=blue},
			]
			coordinates {(2020-01-01,5.5)(2021-01-01,17)};
			\addlegendentry{\acrshort{cri-o}: \textasciitilde{}5.5\% -> 17\%}
			
		\end{axis}
	\end{tikzpicture}  
	
	\caption[Market share of high-level container runtimes between 2020 and 2021.]{Market share of high-level container runtimes between 2020 and 2021, based on \cite{StateContainerKubernetes2020,Sysdig2021Container2021}.}
	\label{fig:container_runtimes_market_share}
\end{figure}

\subsection{DevOps Tool Trends in Cloud Environments}
\label{subsec:devops_tools_trends}
Kubernetes can be combined with a wide range of tools, including tools for monitoring, proxying or the packaging of applications.

 \gls{cicd} tools are amongst the tools integrated most often with Kubernetes. According to the 2018 \cite{CNCFSurvey20182018}, 2019 \cite{CNCFSurvey20192019} and 2020 \cite{CNCFSurvey20202020} \gls{cncf} surveys, Jenkins was and still is the most used \gls{cicd} tool, although its usage decreased from the 70\% in 2018 to 53\% in 2020. The second most used \gls{cicd} tool is GitLab \gls{cicd} with a steady 34-36\% usage since 2019. Other popular \gls{cicd} tools include GitHub Actions, CircleCI, TravisCI and Argo. An exhaustive ranking can be found in the \gls{cncf}’s 2020 survey \cite{CNCFSurvey20202020}.

Ingress providers are reverse proxies for Kubernetes services. According to the 2018 \cite{CNCFSurvey20182018}, 2019 \cite{CNCFSurvey20192019} and 2020 \cite{CNCFSurvey20202020} \gls{cncf} surveys, nginx was and still is the most used ingress provider with a steady +60\% usage. Envoy and HAProxy the second most used ingress providers with 37\% and 27\% usage respectively. Again, an exhaustive ranking can be found in the \gls{cncf}’s 2020 survey \cite{CNCFSurvey20202020}.

Helm has been the primary tool to package Kubernetes applications, with a steady over 60\% usage in all \gls{cncf} surveys. In the 2020 \gls{cncf} survey \cite{CNCFSurvey20202020}, Kustomize emerged as another significant packaging tool with around 16\% usage.

Considering metrics collection tools, report \cite{Sysdig2021Container2021} found Prometheus to be the most used tool with 62\% usage, StatsD with 20\% usage and \gls{jmx} with 19\% usage.

Due to the significant standardization steps taken by the \gls{oci}, a wide range of public container image registries are availabile to be used with Kubernetes. According to report \cite{Sysdig2021Container2021}, the Docker image registry is still the most used container image registry with 36\% usage, followed by the \gls{gcr} with 26\% and Red Hat’s Quay registry with 24\%. An exhaustive ranking can be found in the Sysdig 2021 Container Security and Usage Report \cite{Sysdig2021Container2021}.