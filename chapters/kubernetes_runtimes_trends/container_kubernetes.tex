\section{Base Definitions and Terms}
\label{sec:intro_definitions_terms}
The following organizations, standards, certifications and products need to be introduced before analyzing Kubernetes:

\begin{description}
	
	\item[\textbf{Kubernetes} is defined in the Kubernetes documentation \cite{WhatKubernetes} as follows:] ``\textit{Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation}.''
	
	\item[\textbf{\acrfull{cncf}}] is a project established by the Linux Foundation with the mission of driving the adoption of cloud native computing and supporting an ecosystem of open source, vendor-neutral projects \cite{CloudNativeComputing2018}. The \gls{cncf} provides services and support for member projects and (mostly Kubernetes related) certifications for IT-specialists, service providers and software products. At the time of writing, the \gls{cncf} Landscape consisted of over 1500 projects, over 600 companies and organizations and over 90 certified Kubernetes offerings \cite{CNCFCloudNative}.
	
	\begin{description}
		\item[\textbf{\gls{cni}:}] The \gls{cni} is an \gls{api} specification and corresponding libraries for developing networking plugins for Linux containers, defined by the \gls{cncf} \cite{Cni,ContainerNetworkInterface}. The focus of \gls{cni} plugins is to provide network connectivity between containers. It is relevant, since additionally to the default Kubernetes pod networking, Kubernetes supports third party \gls{cni}-conform network plugins to implement additional networking possibilities \cite{NetworkPlugins}. 
	\end{description}
	
	\item[\textbf{\gls{oci}}] is a project established by the Linux Foundation for creating open industry standards around container formats and runtimes \cite{OpenContainerInitiative}. The \gls{oci} currently consists of three specifications:
	\begin{description}
		\item[\textbf{\gls{runtime-spec}:}] Specifies the lifecycle, execution environment and the configuration of a container \cite{OpenContainerInitiative2020}.
		
		\item[\textbf{\gls{image-spec}:}] Specifies the structure and parts of an \gls{oci}-conform container image \cite{OpenContainerInitiative2020a}. It enables the creation of interoperable tools for building, transporting, and preparing container images for execution.
		
		\item[\textbf{\gls{distribution-spec}:}] Aims to standardize the distribution of container images, the interactions with container image registries and the management of metadata of container repositories \cite{OpenContainerInitiative2020b}. Since the Docker Registry \acrshort{http} \acrshort{api} V2 protocol became the de-facto standard in the container world, it was taken as a basis and reference implementation for this specification \cite{OpenContainerInitiative2018,OpenContainerInitiative2018a}.
		
	\end{description}
	
	\item[\textbf{\gls{cri}}] is the plugin interface defined by Kubernetes which enables it to use a wide variety of container runtimes, without the need to recompile Kubernetes for each container runtime \cite{ContainerRuntimeInterface2020,CRIContainerRuntime2020}.
	
\end{description}


\section{Base Kubernetes Components}
\label{sec:intro_kubernetes_components}

%The names of many Kubernetes components or resources are not specific to Kubernetes and are often widely used in other contexts in the field of computer sciences. Starting with this section, whenever a Kubernetes component or resource is referenced, the name of the component or resource will be written with a \texttt{monospace font}. Although it would be possible to write Kubernetes every time before the name of such a resource (such as Kubernetes node), writing such names with the \texttt{monospace font} was decided to be less cumbersome.

A Kubernetes cluster consists of a control plane (usually consisting of multiple master nodes) and multiple worker nodes. An overview of Kubernetes components is provided in \autoref{fig:kubernetes-components}.

\begin{figure}[h]%[htbp!]
	\centering
	
	\begin{forest}
		for tree={
			align=center,
			%grow tree from left to right
			grow'=0,
			font=\sffamily,
			edge+={thick, -{Stealth[]}},
			l sep'+=10pt,
			fork sep'=10pt,
		},
		forked edges,
		%if level=0{
		%	inner xsep=0pt,
		%	tikz={\draw [thick] (.children first) -- (.children last);}
		%}{},
		[Kubernetes cluster
			[control plane
				[\acrshort{kube-apiserver}]
				[etcd]
				[\acrshort{kube-scheduler}]
				[\acrshort{kube-controller-manager}]
				[\acrshort{cloud-controller-manager}]
			]
			[all nodes
				[kubelet]
				[\acrshort{kube-proxy}]
				[container runtime]
			]
			[addons]
		]
	\end{forest}
	
	\caption[Overview of the main Kubernetes components.]{Overview of the main Kubernetes components.}
	\label{fig:kubernetes-components}
\end{figure}


\begin{description}
	\item[Control plane:] The control plane is responsible for cluster-wide decision making, the overall management of the cluster infrastructure and the handling of cluster events \cite{KubernetesComponents}. Control plane components can run on any node and they can be run redundantly.
	
	\begin{description}
		
		\item[etcd] is the central data storage that holds the cluster configuration, cluster status information and other cluster-wide information \cite{KubernetesComponents}.
				
		\item[\gls{kube-apiserver}] provides communication with the control plane by providing the Kubernetes \gls{api} \cite{APIOverview,KubernetesV119,KubernetesAPIConcepts}. It is the only component that has direct and read/write access to etcd \cite{KubernetesComponents}. Furthermore, it provides communication between the other control plane components and provides communication from the \gls{api} to the control plane.
		
		\item[\gls{kube-scheduler}] is responsible for selecting a Kubernetes node for a new Pod to run on based on factors such as resource requirements, policy constraints, affinity specifications or deadlines \cite{KubernetesComponents}.
		
		\item[\gls{kube-controller-manager}] is responsible for handling Kubernetes node status changes, for the correct number of active Pods, providing endpoints for the Pods and providing default accounts and tokens for new Kubernetes namespaces \cite{KubernetesComponents}.
		
		\item[\gls{cloud-controller-manager}] is responsible for managing and providing features for cloud provider integration \cite{KubernetesComponents}.
	\end{description}
	
	\item[All nodes:] The node components are running on both control plane and worker Kubernetes nodes.
	
	\begin{description}
		\item[kubelet] monitors container health and state and makes certain that the needed number of healthy containers is running in each Pod. The kubelet uses and runs on top of the \textbf{container runtime} installed on the host machine. Thanks to the \gls{cri}, any container runtime which implements it can be used \cite{KubernetesComponents}.
		
		\item[\gls{kube-proxy}] manages the network rules on the Kubernetes nodes and provides service abstraction and proxying \cite{KubernetesComponents}.
	\end{description}
	
	\item[Addons] are used to implement additional cluster features such as cluster \gls{dns}, dashboards, container resource monitoring or cluster-level logging \cite{KubernetesComponents}.
\end{description}

\section{The Kubernetes API and The Main Kubernetes Resources}
\label{sec:intro_kubernetes_api_resources}
The Kubernetes \gls{api} provides a clear schema to represent all concepts and entities which can exist in and are understood by a Kubernetes cluster. Both the Kubernetes components, which communicate through the Kubernetes \gls{api}-server, and the external clients that communicate with the Kubernetes cluster use this \gls{api}. The \gls{api} definition is available in \cite{KubernetesV119}, while corresponding documentation is available in \cite{APIOverview,KubernetesAPIConcepts}. In the following, a base overview of this \gls{api} is given.

Everything in Kubernetes is represented by Kubernetes \gls{api} objects \cite{APIOverview}, which are persistent instances of the resources defined by the schemata of the \gls{api} \cite{UnderstandingKubernetesObjects}. The Kubernetes components work together to converge the actual cluster state into the cluster state defined by all objects.

The main Kubernetes resources include, but are not limited to:
\begin{description}
	\item[Pods] are the atomic units of work managed by Kubernetes \cite{Pods}. Conceptually, a Pod consists of one or multiple tightly coupled containers which cooperate to provide an atomic and cohesive unit of service. Technically, they are processes running in the same context and the same isolated environment. Containers in a Pod can communicate through a loopback interface, while the Pod itself gets its own cluster-internal \gls{ip} address \cite{Service}.
	\begin{description}
		\item[Static Pods] are special Pods for system components, which are managed locally by the kubelet. They are visible for the Kubernetes-\gls{api} through special, read-only mirror-Pods \cite{Pods}. 
	\end{description}
	
	\item[Workloads] represent applications deployed on Kubernetes, consisting of multiple Pods, each representing a component of the application \cite{Workloads}. Apart from the conceptual grouping of Pods, they manage the life-cycle and ensure the correct state of their Pods. Kubernetes provides multiple types of Workloads (Deployment, ReplicaSet, StatefulSet, DaemonSet, Job, CronJob), which all provide different behaviors.
	
	\item[Services] abstract, expose and load-balance multiple identical Pods as a microservice \cite{Service}. This allows to scale Pods without affecting the microservice or change the microservice without affecting the Pods. Another similar Kubernetes resource is the Ingress resource \cite{Ingress}, which will not be discussed here.
	
	\item[ConfigMaps and Secrets] allow to decouple the configuration from the individual container images \cite{ConfigMaps,Secrets}. For both, the data is defined as key-value pairs, which can be injected into the container as files or environment variables \cite{ConfigMaps,Secrets}. ConfigMaps \cite{ConfigMaps} define non-sensitive information, while Secrets \cite{Secrets} define sensitive information.
	
	\item[Volumes] provide an abstraction over different storage resources which are mounted into containers of a given Pod \cite{Volumes}. Volumes can bind a wide range of storage technologies (including \gls{nfs} or \gls{iscsi}) and also certain Kubernetes resources like Secrets or ConfigMaps. All Volumes survive container restarts, but only \textbf{PersistentVolumes} exist outside of the lifetime of a given Pod. PersistentVolumes are selected based on \textbf{PersistentVolumeClaim} resources, which define parameters of the required storages and bind them to the given Pods \cite{PersistentVolumes}.
	
	\item[Namespaces] provide the separation between workloads and objects of different users and projects. They can be thought of as virtual clusters, although they do not provide complete isolation to other namespaces. Most (but not all) Kubernetes objects are namespace-scoped \cite{Namespaces}.
\end{description}

Further Kubernetes resources are described where required in the following chapters. Previously mentioned Kubernetes objects as well as objects discussed in later sections can be seen in \autoref{fig:student_project_visualization}, which provides an overview of the relationships and interactions of these objects in a real project.

\section{Authentication, Authorization and Policies in Kubernetes and OKD/OCP}
\label{subsec:intro_authentication_authorization_policies}

\subsection{Authentication}
In Kubernetes there is a distinction between \textbf{User} accounts for human users and \textbf{ServiceAccounts} under which the workloads are executed \cite{ManagingServiceAccounts}. ServiceAccounts exist in and are managed by the Kubernetes \gls{api}.

On the other hand, User accounts are not defined in and cannot be created by the Kubernetes \gls{api}, they must be managed by external services \cite{Authenticating}. The user must authenticate to an external service, which returns a token or certificate with which the user can access the Kubernetes \gls{api} \cite{Authenticating}. This token however is not bound to an identity in the Kubernetes \gls{api}, the mere presence of a valid token or certificate authenticates the request. After and during authentication, Kubernetes determines the identity (username, \gls{uid}, groups, etc.) which correspond to the presented token. The exact procedure of matching the token with an identity depends on the used authentication system.

\subsection{Authorization}
After authentication and identity provision, the request goes through multiple authorization stages until it is accepted and stored in the cluster's etcd database.

Kubernetes can use different authorizers (also called authorization modules or authorization modes) \cite{AuthorizationOverview} to authorize requests to the \gls{api}. Authorizers include the Node, the \gls{abac}, the \gls{rbac} and the Webhook modules. In this thesis, only the \gls{rbac} authorization mode \cite{UsingRBACAuthorization} is discussed. Authorizers have a clear limit of responsibility: they only verify that the given User and ServiceAccount is allowed to do the requested operations to the requested cluster resource types, but they do not verify the configurations of the requested objects.

Using \gls{rbac}, policies are defined as Role or ClusterRole objects, each consisting of one or multiple rules which define the allowed operations on the different \gls{api} resource types \cite{UsingRBACAuthorization}. These Roles are bound to Users or ServiceAccounts using RoleBindings and ClusterRoleBindings. The \gls{rbac} authorizer authorizes the request against the matching policies. 

After the request is authenticated and authorized, depending on the cluster's configuration, it is intercepted by different admission controllers \cite{UsingAdmissionControllers} before it is persisted into the etcd database. The admission controllers validate and/or modify the configurations of the requested objects. They may validate the objects against different policies (not to be confused by the policies defined for authorizers) or modify their definitions by injecting or modifying different parameters.

Admission controllers allow to create fine-grained policies that govern the resources and capabilities available for different Pods. In standard Kubernetes and many Kubernetes distributions (see \autoref{subsec:intro_kubernetes_distributions}) these policies are defined as \glspl{psp} \cite{PodSecurityPolicies}, while \gls{ocp} and \gls{okd} (two Kubernetes distributions) use a custom admission controller and policy type, the \gls{scc} \cite{ManagingSecurityContext}. Although there are differences between the two, they are similar in functionality and can validate and modify Pod definitions to enforce fine-grained Pod-related security policies. Similar policy types such as Security Contexts \cite{ConfigureSecurityContexta} or the \gls{opa} Gatekeeper \cite{zhangOPAGatekeeperPolicy2019} are not discussed in this thesis.

\subsection{Further Policies in Kubernetes}
Limiting the resource consumption of Kubernetes objects is also implemented through different policy types and their admission controllers. Resource Quotas \cite{ResourceQuotas} can be used to limit the number of objects and the total resource consumption of all objects in a given namespace. The limits for individual Pods and their containers can be managed using Limit Ranges \cite{LimitRanges}. For a complete list of additional Pod-level policy types, see the corresponding page of the documentation \cite{Policies}.

\section{Container Runtimes in Kubernetes}
\label{sec:intro_container_runtimes}

Although in the beginning Kubernetes only supported Docker as container runtime, it now supports a great variety of container runtimes allowing various different configurations, as shown in \autoref{fig:runtimes}. This great flexibility is due to the three \gls{oci} specifications (\gls{runtime-spec}, \gls{image-spec} and \gls{distribution-spec}), the \gls{cni} specification, and the \gls{cri} defined by Kubernetes itself. These specifications all standardize different crucial aspects of container technology.

Before analyzing the different container runtimes supported by Kubernetes, a short definition of the term ``container runtime'', as well as ``low-'' and ``high-level container runtime'' is required. Since there is no exact definition to what a container runtime is and what exactly are and are not tasks or capabilities of a container runtime, this thesis provides the definitions which will be used further on. The definition of container runtime is divided into two sub-definitions, based on the scope of functionality provided by the container runtime. A \textbf{low-level container runtime} is focused on running containers and may also offer low-level tools for managing cgroups, namespaces and root filesystems. A \textbf{high-level container runtime} builds on a low-level container runtime. It interacts with the container registry, fetches or publishes container images and provides tools to create, modify or extract container images. During operation, the container image is created or retrieved by a high-level runtime, which also extracts the container image so that the low-level container runtime can execute it. These definitions are based on the definitions of Lewis \cite{lewisContainerRuntimesPart}, although they can also be found throughout the literature in similar forms.

\begin{description}
	\item[High-level runtimes] integrate into Kubernetes mostly through the \gls{cri}, the implementation of the \gls{cri} is therefore mandatory for Kubernetes support.
	
	At the time of writing, Kubernetes supports the following high-level runtimes:
	
	\begin{description}
		\item[\acrshort{cri-o}] aims to be a lightweight alternative to Docker or containerd. It implements both the \gls{cri} and the \gls{oci} specifications, therefore allowing Kubernetes to use any \gls{oci}-compliant low-level runtime \cite{Crio}.
		
		\item[Docker] is a container platform consisting of multiple tools, which also include a container runtime. Docker was the default runtime for Kubernetes, but as both projects matured, Kubernetes became more flexible and Docker was separated into multiple components, some of which are also used outside of Docker, such as containerd or runc \cite{ContainerdJoinsCloud2017}. Docker interfaces with the kubelet using the dockershim, a \gls{cri} support module built into the kubelet for Docker support. With the deprecation of the dockershim \cite{Kubernetes20Changelog}, Kubernetes distributions are either dropping Docker support, or at least choosing \acrshort{cri-o} or containerd as the preferred runtime. Although the market is moving away from Docker, Mirantis is developing a \gls{cri} implementation for Docker, the cri-dockerd plugin \cite{Cridockerd2021}, to enable the further usage of Docker as a high-level runtime for Kubernetes.
		
		\item[containerd] is a widely used container runtime with an emphasis on simplicity, robustness and portability which is also the high-level runtime used inside Docker. containerd by itself does not implement the \gls{cri}, Kubernetes integration is achieved through the cri-containerd plugin \cite{Cricontainerd}. Since containerd supports the \gls{oci} \gls{runtime-spec}, it can work with any low-level \gls{oci} runtime. containerd also specifies its own \gls{api}, the containerd ShimV2 \acrshort{api} \cite{ContainerdRuntimeV2}, which can be implemented by low level runtimes for (non-OCI) containerd integration and provides extended and enhanced functionalities \cite{CRIShimV2New}.
		
		\item[Frakti] is a lightweight high-level runtime that builds on Kata Containers \cite{Frakti2020}. It interfaces with the kubelet using the \gls{cri} and interfaces with Kata Containers directly, since it does not implement the \gls{oci} specifications.
		
		\item[LXE] is a work-in-progress \gls{cri} implementation for \gls{lxd} \cite{agLXE2021}. It has not gained significant traction and development also keeps stopping for months, so whether it will reach the stable state remains uncertain. Nevertheless, bringing \gls{lxd} support under Kubernetes would be an exciting development and addition to Kubernetes. The distinction between \gls{lxd} and \gls{lxc} usually causes a lot of confusion. To address this issue in a compact manner, \gls{lxd} is a daemon providing enhanced functionality which binds to \gls{lxc}'s libraries to manage \gls{lxc} containers \cite{RelationshipLXC}.
	\end{description}
	
	\item[Low-Level Runtimes:] Apart from the low-level runtimes directly supported by certain high-level runtimes, the range of possible low-level runtimes is further increased due to the \gls{oci} specifications. containerd and \acrshort{cri-o} should support all low-level \gls{oci} runtimes.
	
	\begin{description}
		\item[runc] is the low-level runtime of Docker, which became the \gls{oci} reference implementation after it was donated to the \gls{oci} \cite{Runc2021}.
		
		\item[crun] aims to be a faster and lighter alternative to runc by being implemented in C instead of Go \cite{Crun2021a}.
		
		\item[Kata Containers] is a low-level runtime providing light \gls{kvm} instances instead of containers, by using either \gls{qemu} or Firecracker as an underlying runtime. It provides an \gls{oci} implementation, but is also accessible directly through Frakti and through the ShimV2 \acrshort{api} by containerd \cite{KataContainersArchitecture2021}.
		
		\item[Firecracker] is a lightweight alternative to \gls{qemu} \cite{Firecracker2021}. It is also accessible by containerd directly, not only through Kata Containers, as it has its own ShimV2 \acrshort{api} implementation \cite{Firecrackercontainerd2021a}.
		
		\item[Nabla] is a runtime launched by IBM which aims to reduce the attack surface of the host kernel. The system calls of a Nabla container are linked to a LibraryOS unikernel which is restricted to only seven syscalls, thus reducing the attack surface \cite{NablaContainersNew}. The \gls{oci} implementation is provided by the Nabla’s runnc runtime \cite{Runnc2020}.
		
		\item[gVisor] also aims to reduce the attack surface of containers by intercepting system calls through the gVisor application kernel \cite{GVisorDocumentation}. In contrast to Nabla, it implements a significant part of the Linux system call interface and has some differences in design and implementation \cite{lumDiscussingExploitationPriv2018}. Apart from the ShimV2 \acrshort{api} containerd integration \cite{GVisorShimOverview}, an \gls{oci} implementation is also provided by the runsc runtime \cite{GVisorRunsc}.
		
		\item[Sysbox] started as a fork of runc and was developed into a light, security focused low-level runtime for system containers \cite{Sysbox2021}. It implements the \gls{oci} \gls{api}, but breaks some requirements of the \gls{api} in favor of an enhanced Docker compatibility \cite{SysboxOCICompatibility2021}. By being designed for system containers, it aims to be more lightweight than the light \glspl{vm} provided by Kata Containers or Firecracker while providing the benefits of system containers (such as \gls{lxc}/\gls{lxd}). Furthermore, it claims to solve the issues experienced with many container runtimes when trying to run nested containers or privileged containers inside unprivileged containers \cite{SysboxUserGuide2021}. 
	\end{description}
\end{description}

\begin{figure}[h]%[!htbp]
	\centering
	\includegraphics[width=1\textwidth]{runtimes_v3.1.png}
	%\rotatebox{90}{\includegraphics[width=1\textwidth]{runtimes_v2.png}}
	\caption[Available Kubernetes runtimes and their relationships.]{Available Kubernetes runtimes and their relationships based on \cite{carrezKubernetesDemystifyingContainer2020}. This view includes additional runtimes and other modifications to reflect the current state of Kubernetes runtimes.}
	\label{fig:runtimes}
\end{figure}


\section{Kubernetes Distributions}
\label{subsec:intro_kubernetes_distributions}
Many vendors provide Kubernetes distributions which are prepackaged software platforms built around Kubernetes. These are often preferred instead of plain Kubernetes since they usually provide simplified installation, additional bundled tools and enterprise support possibilities. Kubernetes distributions may provide special distribution specific host \glspl{os}, additional custom Kubernetes components and other enterprise features such as out-of-the-box identity provider integration.
