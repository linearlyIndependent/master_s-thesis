\section{Experimental Setup}
\label{sec:test_cluster_evaluation_setup}

All practical parts of this paper were carried out at the Faculty of Computer Science of the \gls{uas} Technikum Wien. The physical and virtual network infrastructure, as well as the servers and other resources were kindly provided by the faculty. This work was carried out for and supported by the faculty's interest in building a Kubernetes cluster to support its own future and existing projects.

\subsection{The Underlying Infrastructure}
In the following, an overview of the relevant parts of the faculty's network infrastructure and hardware resources should be given.

The faculty manages its own server landscape, own internal networks and own set of services to support its diverse IT-infrastructure needs. This infrastructure however is not completely isolated and is interconnected with other parts of the university's network infrastructure.

\subsubsection{Resources, services and infrastructure managed centrally by the university:}
\begin{itemize}
	\item Managing the cabling between all rooms, inside and outside of the faculty.
	\item Managing switching, routing and ensuring network connectivity until the perimeters of the faculty's server infrastructure.
	\item Providing \gls{wan} connectivity through the university's firewall.
	\item Providing \gls{dns}, \gls{dhcp}, \gls{vlan}s, \gls{vpn} access and other services outside the faculty server infrastructure.
\end{itemize}

\subsubsection{Resources, services and infrastructure managed internally by the faculty:}
\begin{itemize}
	\item Patching and cabling of devices inside faculty premises.
	\item Managing the switching, routing and ensuring network connectivity inside deparment premises.
	\item Managing the private faculty networks, such as lab, storage or cluster networks.
		\begin{itemize}
			\item Firewalls and reverse proxies from the university network to the faculty network.
			\item \gls{dhcp}, \gls{vlan}s and \gls{pxe} on these networks.
		\end{itemize}
	
	\item Managing the child \gls{dns} zone and subdomain of the faculty.
	\item Hosting the projects and websites of the faculty.
	\item Managing the faculty server landscape, including the faculty Proxmox clusters.
\end{itemize}

\subsubsection{The Faculty Proxmox Clusters}
A substantial amount of faculty services and projects are running on the faculty's two Proxmox clusters.

Proxmox \cite{ProxmoxVEFeatures} is a Debian-based, open-source server virtualization platform supporting \gls{lxc} containers and \gls{kvm} \gls{vm}s. It provides backup, template and live-migration functionalities, support for different storage technologies, a firewall, optional enterprise support, as well as a web-interface covering most of its functionality. Multiple Proxmox nodes are connected to a high availability, quorum-based cluster. Further information on Proxmox can be found on the product datasheet \cite{ProxmoxVEFeatures} and in the administration guide \cite{ProxmoxVEAdministration}.

The faculty manages two Proxmox clusters, each consisting of 5 servers and multiple \gls{nas} devices. The servers are different models and have different configurations. The test Kubernetes clusters were deployed on top of \gls{kvm} \gls{vm}s, each cluster on a separate server of the Proxmox cluster. All servers are connected to the university network, as well as to internal storage and lab networks. \autoref{fig:proxmox-cluster} shows the cluster's relevant network architecture.

\begin{figure}[h!]
	\centering
	\resizebox{0.73\textwidth}{!}{
	\begin{tikzpicture}[
		start chain=going right,
		diagram item/.style={
			on chain,
			join
		}
		]
		\node [
		diagram item,
		label=left:WAN/Internet
		] {\includesvg[width=0.075\columnwidth]{cloud}};
		
		\node [
		diagram item,
		label=right:University firewall
		] {\includesvg[width=0.075\columnwidth]{c_firewall_red}};
		
		\node [
		continue chain=going below,
		diagram item,
		label=right:Unknown amount of routers
		] {\includesvg[width=0.075\columnwidth]{c_router_blue}};
		
		\node [
		continue chain=going left,
		diagram item,
		label=above:Server room switch
		] {\includesvg[width=0.075\columnwidth]{c_switch_blue}};
		
		\begin{scope}[start branch=cl1]
			\node [
			continue chain=going below left,
			diagram item,
			label=below:Cluster 1,
			] {\includesvg[width=0.075\columnwidth]{sq_servercluster_green}};
			
			\begin{scope}[start branch=cl1n1]
				\node [
				continue chain=going above,
				diagram item,
				label=below:Node 1,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
			\begin{scope}[start branch=cl1n2]
				\node [
				continue chain=going above left,
				diagram item,
				label=below:Node 2,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
			\begin{scope}[start branch=cl1n3]
				\node [
				continue chain=going left,
				diagram item,
				label=below:Node 3,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
			\begin{scope}[start branch=cl1n4]
				\node [
				continue chain=going below left,
				diagram item,
				label=below:Node 4,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
			\begin{scope}[start branch=cl1n5]
				\node [
				continue chain=going below,
				diagram item,
				label=below:Node 5,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
		\end{scope}
		
		
		\begin{scope}[start branch=cl2]
			\node [
			continue chain=going below right,
			diagram item,
			label=below:Cluster 2,
			] {\includesvg[width=0.075\columnwidth]{sq_servercluster_green}};
			
			\begin{scope}[start branch=cl2n1]
				\node [
				continue chain=going left,
				diagram item,
				label=below:Node 1,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
			\begin{scope}[start branch=cl2n2]
				\node [
				continue chain=going below left,
				diagram item,
				label=below:Node 2,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
			\begin{scope}[start branch=cl2n3]
				\node [
				continue chain=going below,
				diagram item,
				label=below:Node 3,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
			\begin{scope}[start branch=cl2n4]
				\node [
				continue chain=going below right,
				diagram item,
				label=below:Node 4,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
			\begin{scope}[start branch=cl2n5]
				\node [
				continue chain=going right,
				diagram item,
				label=below:Node 5,
				] {\includesvg[width=0.075\columnwidth]{c_server}};
			\end{scope}
			
		\end{scope}
		
		
		
		%\node [
		%continue chain=going below,
		%diagram item,
		%label=below:BELOW
		%] {\includesvg[width=0.075\columnwidth]{sq_firewall_red}};
		
	\end{tikzpicture}
	}

	\caption{The faculty Proxmox cluster.}
	\label{fig:proxmox-cluster}
\end{figure}

\subsection{The Kubernetes Cluster Test Architecture}
The abstraction of the underlying hardware resources and the \gls{lxc} and \gls{kvm} support provided by the Proxmox clusters make a suitable environment for the virtualization of multiple Kubernetes clusters. 

It was considered to use \gls{lxc} containers to host the Kubernetes nodes for lower resource consumption. After considering possible complications with networking and stacked virtualization as well as security implications like having to run privileged containers with the unconfined apparmor profile (as described on the Ubuntu Blog \cite{graberRunningKubernetesLXD2017}), this option was rejected in favor of \gls{kvm} \gls{vm}s.

To enable a fair evaluation of Kubernetes distributions, each Kubernetes distribution was deployed into the same test architecture built on top of \gls{kvm} \gls{vm}s. The test architecture is based on the architecture used as part of an episode \cite{robinsonOpenShiftCommonsBuilding} of the OpenShift Commons lecture series, with the corresponding GitHub repository being available at \cite{robinsonCragrOkd4Files}. The architecture had to be adapted for Proxmox instead of VMWare and for \gls{okd} and \gls{ocp} minor modifications were also required to fit the different project environment. More modifications and additional components were required for the Rancher and standard Kubernetes clusters, but the main architecture and the components used remained the same. 

Each Kubernetes cluster was created on a separate server of the Proxmox cluster for a more evenly distributed resource consumption. Since each Kubernetes distribution has slightly different hardware requirements, parameters like the exact amount of memory, processor cores or storage space varied between the test clusters. For each distribution, the official hardware requirements were followed. Also since the servers in the Proxmox clusters have different processors, the clock-speed and other \gls{cpu} parameters also varied between the test architectures.

For each test architecture a private virtual network was created on top of a Linux bridge device. The private network had the subnet 172.30.1.0/24 and the private \gls{dns} zone ``<cluster>.cs.lan''. On this subnet the pfSense firewall was the only device with a second network interface and direct access to the university's networks and the \gls{wan}.

The test architecture consisted of the following nodes and components (see also \autoref{tab:test-architecture}):

\begin{description}
	\item[The firewall] (pfSense 2.4.5) \gls{vm} with one interface to the private subnet and one interface to a university network. It served as a default gateway and \gls{dhcp} server on the private network, provided \gls{nat}-ing and served as a reverse proxy for making the Kubernetes distribution's dashboard accessible from the outside.
	
	\item[The service node] was a CentOS 7 \gls{vm} with the GNOME desktop environment. It served as a general service node for configuring the firewall and for other tasks during and after the Kubernetes cluster deployment. The \gls{vm} also ran multiple servers vital for the operation of the Kubernetes cluster. Firstly, it hosted the authoritative \gls{bind} \gls{dns} server for the private \gls{dns} zone / subdomain ``<cluster>.cs.lan'' (see: \autoref{tab:test-architecture}) and provided name resolution for all nodes on the network. Secondly, it provided the HAProxy load-balancer for the Kubernetes cluster. Thirdly, it provided an Apache \gls{http} server to host the \gls{os} images, configuration files and ignition files required for the \gls{okd} and OpenShift installation.
	
	\item[The boostrap node] is required to set up the \gls{okd} and \gls{ocp} clusters and was removed after installation \cite{InstallingClusterBare}.
	
	\item[Three control plane nodes] which is the minimum number required for the control plane of a \gls{ha} Kubernetes cluster so that the nodes are able to establish a quorum (see documentation \cite{OptionsHighlyAvailable}). The control plane also includes the etcd databases which hold the configuration and other control plane information for the cluster. For the test architecture, the stacked etcd topology was used, where each control plane node also hosts an instance of the etcd database. This might make the cluster less resilient (etcd nodes are bound to control plane nodes), but for the test topology it was found to be suitable since it allowed to save the resources of three additional virtual machines. The amount of \gls{cpu} cores, memory and storage provided was chosen based on the requirements of the specific distribution.
	
	\item[Two worker nodes] which is one less than the three worker nodes required for a completely \gls{ha} deployment in the Kubernetes documentation \cite{CreatingHighlyAvailable}. As part of a usecase, each cluster was later expanded to three worker nodes (see \autoref{subsec:test_cluster_results_scalability}). The decision to establish \gls{ha} only for the control plane and not for the worker nodes had also the aim to reduce resource consumption. Since only test projects were planned to deployed on the clusters, the lower level of resilience was found to be an acceptable compromise. As for the high-level container runtime, for each distribution, \acrshort{cri-o} was chosen in cases where a choice was possible. Similarly to the control plane nodes, the amount of \gls{cpu} cores, memory and storage provided was chosen based on the requirements of the specific distribution.
\end{description}

A table-based summary of the nodes and the used software versions can be found in \autoref{tab:test-architecture} and \autoref{tab:software-versions}. A network topology of the test architecture is available in \autoref{fig:test_architecture_network_topology}.

\begin{table}[!h]
	\centering
%	\resizebox{\textwidth}{!}{%
	\begin{scriptsize}
	\begin{tabular}{@{}p{0.15\textwidth}p{0.13\textwidth}p{0.245\textwidth}p{0.155\textwidth}p{0.045\textwidth}p{0.06\textwidth}p{0.065\textwidth}@{}}
			\toprule
			\textbf{Name} \mbox{(*.\textit{<cluster>}.cs.lan)} & \textbf{\acrshort{ip}-address} & \textbf{Roles} & \textbf{\acrshort{os}} & \textbf{\acrshort{cpu}} cores & \textbf{Memory} (GiB) & \textbf{Storage} (GiB) \\ \midrule
			cl1-\textit{<dist>}-fw & 172.30.1.1/24, external \acrshort{ip} & Firewall, \acrshort{dhcp}, \acrshort{nat}, reverse-proxy. & FreeBSD (pfSense) & 1 & 1 & 8 \\
			cl1-\textit{<dist>}-svc & 172.30.1.250/24 & \acrshort{dns}, load-balancer, \acrshort{nfs}, \acrshort{http} server. & CentOS 7 (with GNOME) & 2 & 3 & 100 \\
			cl1-\textit{<dist>}-bs & 172.30.1.25/24 & Kubernetes bootstrap node.* &  &  &  &  \\
			cl1-\textit{<dist>}-cp-n1 & 172.30.1.20/24 & Kubernetes control plane node 1. &  &  &  &  \\
			cl1-\textit{<dist>}-cp-n2 & 172.30.1.21/24 & Kubernetes control plane node 2. &  &  &  &  \\
			cl1-\textit{<dist>}-cp-n3 & 172.30.1.22/24 & Kubernetes control plane node 3. &  &  &  &  \\
			cl1-\textit{<dist>}-work-n1 & 172.30.1.23/24 & Kubernetes worker node 1. &  &  &  &  \\
			cl1-\textit{<dist>}-work-n2 & 172.30.1.24/24 & Kubernetes worker node 2. &  &  &  &  \\ \bottomrule
		\end{tabular}%
	\end{scriptsize}
%	}

 	\caption*{
 		
 		%\resizebox{\textwidth}{!}{%
		% Table note table, bit hacky but it works.
		\begin{footnotesize}
		\begin{tabular}{@{}p{0.95\textwidth}@{}}
			%\toprule
			\textbf{*:} Required for the \gls{okd}/\gls{ocp} installation.\\
			\textbf{\textit{<cluster>}:} Cluster name as part of the subdomain. ``k8s'' for standard Kubernetes, ``okd'' for \gls{okd},``ocp'' for \gls{ocp}, ``rke'' for Rancher with \gls{rke}.\\
			\textbf{\textit{<dist>}:} Distribution name as part of hostname. ``k8s'' for standard Kubernetes, ``okd4'' for \gls{okd}, ``ocp4'' for \gls{ocp}, ``rke'' for Rancher with \gls{rke}.\\
			%\bottomrule
		\end{tabular}
		\end{footnotesize}
	}

	\caption[Components of the test architecture.]{Components of the test architecture, including Kubernetes nodes and servers for required services.}
	\label{tab:test-architecture}
\end{table}


\begin{table}[!h]
	\centering
	\resizebox{\textwidth}{!}{%
	\begin{tabular}{@{}lll@{}}
		\toprule
		\textbf{Role} & \textbf{Software} & \textbf{Version} \\ \midrule
		Firewall, \acrshort{dhcp}, \acrshort{nat}, reverse-proxy & pfSense & 2.4.5 \\
		\acrshort{dns} server & \acrshort{bind} & 9.11.4 \\
		load-balancer & HAProxy & 1.5.18 \\
		\acrshort{http} server & Apache & 2.4.6 \\
		k8s Kubernetes node (\acrshort{os}) & Ubuntu Server & 20.04.02 \\
		k8s Kubernetes node (Kubernetes) & standard Kubernetes & 1.19.8 \\
		\gls{okd} Kubernetes node (\gls{os}) & \gls{fcos} & 33.20210104.3 \\
		\gls{okd} Kubernetes node (distribution) & \gls{okd} & 4.6.0-0.okd-2021-01-23-132511 \\
		\gls{okd} Kubernetes node (Kubernetes) & Kubernetes & 1.19.4 \\
		\gls{ocp} Kubernetes node (\gls{os}) & \gls{rhcos} & 4.7.13 \\
		\gls{ocp} Kubernetes node (distribution) & \gls{ocp} & 4.7.18 \\
		\gls{ocp} Kubernetes node (Kubernetes) & Kubernetes & 1.20.0 \\
		\gls{rke} Kubernetes node (\gls{os}) & Ubuntu Server & 20.04.02 \\
		\gls{rke} Kubernetes node (distribution) & Rancher{\textbackslash}\gls{rke} & 2.5.7 \textbackslash{} 1.2.7 \\
		\gls{rke} Kubernetes node (Kubernetes) & Kubernetes & 1.19.9 \\
		\gls{rke} Kubernetes node (distribution) & \gls{rke} & 1.2.7 \\
		\gls{rke} Kubernetes node & Docker & 20.10.5 \\
		Virtualization & Proxmox & 6.0 \\
		\bottomrule
	\end{tabular}
	}
	\caption{Name and version of Software used for different roles.}
	\label{tab:software-versions}
\end{table}

\begin{figure}[!h]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{tikzpicture}[
			start chain=going above,
			diagram item/.style={
				on chain,
				join
			}
			]
			\node [
			diagram item,
			label=left:Server \acrshort{nic} (uni. network)
			] {\includesvg[width=0.075\columnwidth]{tj45}};
			
			\node [
			diagram item,
			label=left:Linux bridge
			] {\includesvg[width=0.075\columnwidth]{sq_hub_blue}};
			
			\node [
			continue chain=going right,
			diagram item,
			label=below:cl1-<dist>-fw
			] {\includesvg[width=0.075\columnwidth]{sq_firewall_red}};
			
			\begin{scope}[start branch=vmbr100]
				\node [
				continue chain=going below right,
				diagram item,
				label=above:Linux bridge
				] {\includesvg[width=0.075\columnwidth]{sq_hub_blue}};
				
				\begin{scope}[start branch=svc]
					\node [
					continue chain=going left,
					diagram item,
					label=below:cl1-<dist>-svc,
					] {\includesvg[width=0.075\columnwidth]{sq_loadbalancer}};
				\end{scope}
				
				\begin{scope}[start branch=bs]
					\node [
					continue chain=going below left,
					diagram item,
					label=left:cl1-<dist>-bs,
					] {\includesvg[width=0.075\columnwidth]{sq_vm}};
				\end{scope}
				
				\begin{scope}[start branch=work-n1]
					\node [
					continue chain=going below,
					diagram item,
					label=below:cl1-<dist>-work-n1,
					] {\includesvg[width=0.075\columnwidth]{sq_vm}};
				\end{scope}
				
				\begin{scope}[start branch=work-n2]
					\node [
					continue chain=going below right,
					diagram item,
					label=right:cl1-<dist>-work-n2,
					] {\includesvg[width=0.075\columnwidth]{sq_vm}};
				\end{scope}
				
				\begin{scope}[start branch=cp-n1]
					\node [
					continue chain=going right,
					diagram item,
					label=right:cl1-<dist>-cp-n1,
					] {\includesvg[width=0.075\columnwidth]{sq_vm}};
				\end{scope}
				
				\begin{scope}[start branch=cp-n2]
					\node [
					continue chain=going above right,
					diagram item,
					label=right:cl1-<dist>-cp-n2,
					] {\includesvg[width=0.075\columnwidth]{sq_vm}};
				\end{scope}	
				
				\begin{scope}[start branch=cp-n3]
					\node [
					continue chain=going above,
					diagram item,
					label=above:cl1-<dist>-cp-n3,
					] {\includesvg[width=0.075\columnwidth]{sq_vm}};
				\end{scope}
				
			\end{scope}
			
		\end{tikzpicture}
	}
	
	\caption[Network topology of the test architecture.]{Network topology of the test architecture. Devices are connected using a Linux bridge device, the firewall \gls{vm} accesses the university network using another bridge device.}
	\label{fig:test_architecture_network_topology}
\end{figure}
