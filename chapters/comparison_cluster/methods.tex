\section{Methods}
\label{sec:test_cluster_evaluation_methods}

For practical evaluation, the three Kubernetes distributions found most suitable in \autoref{subsec:comparison_matrix_results_selection}, \gls{okd}, \gls{ocp} and Rancher were evaluated on similar test architectures. Additionally, standard Kubernetes was also evaluated to establish a reference and a baseline of functionalities. Based on the reference architecture described in \autoref{sec:test_cluster_evaluation_setup}, each Kubernetes distribution was deployed on a separate node of the faculty Proxmox cluster.

Each Kubernetes distribution was installed in a weeks time. Since these time periods also included work hours spent on other projects, more precise durations cannot be provided. It has to be noted that the distributions were installed one-at-a-time and their installation processes all share certain steps. Lessons learned from the first installation were applied to the later ones, which therefore shortened their install time. A comparison of the install times is therefore by itself not meaningful.

%\subsection{The Firewall Configuration}
%\label{subsec:test_cluster_methods_firewall_configuration}

%\subsection{The Service Node Configuration}
%\label{subsec:test_cluster_methods_service_node_configuration}

\subsection{The Standard Kubernetes Installation}
\label{subsec:test_cluster_methods_kubernetes_installation}

For the deployment, an instance of the test architecture defined in \autoref{fig:test_architecture_network_topology} was created with the exact parameters defined in \autoref{tab:test-architecture-k8s}.

The standard Kubernetes cluster was deployed using kubeadm, an official Kubernetes deployment tool aimed at streamlining Kubernetes cluster deployment on already existing host nodes. For the deployment, the official Kubernetes kubeadm deployment documentation \cite{BootstrappingClustersKubeadm} and the kubeadm \gls{ha} deployment documentation was followed \cite{CreatingHighlyAvailable}. All steps not referenced to other sources can be attributed to these documentations.

The steps taken during the installation can be summarized as follows:
\begin{enumerate}
	\item cl1-k8s-cp-n1 was installed and configured with Ubuntu server 20.04. Here, the following steps were executed, according to the pre-deployment tasks defined in \cite{InstallingKubeadm}:
	\begin{enumerate}
		\item Disabled swap, loaded the required kernel modules and checked the local firewall for necessary ports.
		\item Installed a high-level container runtime, in this case \gls{cri-o} with the low-level runtime runc according to the Kubernetes runtimes documentation \cite{ContainerRuntimesCRIO}.
		\item Installed kubeadm, kubelet and kubectl and pinned their package versions.
	\end{enumerate}
	\item The configured node was cloned 4 times to create all control plane and worker nodes. Hostname and other parameters were adjusted on the cloned nodes.
	\item On one of the selected control plane nodes, the init parameters for kubeadm were defined. The parameters can be passed by using command line flags or as a config file. For this deployment, the config file option was used, so that the init config file can be reused for later deployments.
	\begin{enumerate}
		\item Default configuration was generated according to the documentation \cite{KubeadmInitUsing} and the file was edited with the parameters of the planned cluster.
		\item As for the range of valid parameters and value types the kubeadm \gls{api} documentation \cite{KubeadmAPITypes} was consulted.
		\item The control plane was initialized as described in \cite{CreatingHighlyAvailable}, with the difference that the config file was with passed with the \texttt{--config} flag.
	\end{enumerate}
	\item The \gls{cni} networking plugin was installed on the node, in this case Weave Net according to its documentation \cite{IntegratingKubernetesAddon}.
	\item The other two control plane nodes were joined by executing the join command previously returned by the kubeadm init command.
	\item The two worker nodes were joined by executing the same join command, but without the \texttt{--control-plane} flag.
	\item The Kubernetes dashboard was set up according to the documentation \cite{WebUIDashboard}.
	
\end{enumerate}

\begin{table}[!h]
	\centering
%	\resizebox{\textwidth}{!}{%
	\begin{scriptsize}
		\begin{tabular}{@{}p{0.13\textwidth}p{0.13\textwidth}p{0.21\textwidth}p{0.20\textwidth}p{0.055\textwidth}p{0.06\textwidth}p{0.065\textwidth}@{}}
			\toprule
			\textbf{Name} \mbox{(*.k8s.cs.lan)} & \textbf{\acrshort{ip}-address} & \textbf{Roles} & \textbf{\acrshort{os}} & \textbf{\acrshort{cpu} cores} & \textbf{Memory} (GiB) & \textbf{Storage} (GiB) \\ \midrule
			cl1-k8s-fw & 172.30.1.1/24, external \acrshort{ip} & Firewall, \acrshort{dhcp}, \acrshort{nat}, reverse-proxy. & FreeBSD (pfSense) & 1 & 1 & 8 \\
			cl1-k8s-svc & 172.30.1.250/24 & \acrshort{dns}, load-balancer, \acrshort{nfs}, \acrshort{http} server. & CentOS 7 (with GNOME) & 2 & 3 & 100 \\
			%cl1-k8s-bs & 172.30.1.25/24 & Kubernetes bootstrap node.\^ &  &  &  &  \\
			cl1-k8s-cp-n1 & 172.30.1.20/24 & Kubernetes control plane node 1. & Ubuntu Server 20.04& 2 & 6 & 50 \\
			cl1-k8s-cp-n2 & 172.30.1.21/24 & Kubernetes control plane node 2. & Ubuntu Server 20.04& 2 & 6 & 50 \\
			cl1-k8s-cp-n3 & 172.30.1.22/24 & Kubernetes control plane node 3. & Ubuntu Server 20.04& 2 & 6 & 50 \\
			cl1-k8s-work-n1 & 172.30.1.23/24 & Kubernetes worker node 1. & Ubuntu Server 20.04& 2 & 6 & 50 \\
			cl1-k8s-work-n2 & 172.30.1.24/24 & Kubernetes worker node 2. & Ubuntu Server 20.04& 2 & 6 & 50 \\ 
			\bottomrule
		\end{tabular}%
	\end{scriptsize}
%	}
	
	\caption[Components of the standard Kubernetes test architecture.]{Components of the standard Kubernetes test architecture, including Kubernetes nodes and servers for required services. Further specification of used software can be found in \autoref{tab:software-versions}.}
	\label{tab:test-architecture-k8s}
\end{table}


\subsection{The OKD and OCP Installation}
\label{subsec:test_cluster_methods_okd_installation}

% Before starting the installation, firstly the firewall and the service node were configured as described in \autoref{subsec:test_cluster_methods_firewall_configuration} for the firewall and in \autoref{subsec:test_cluster_methods_service_node_configuration} for the service node.

For the deployment, two instances of the test architecture defined in \autoref{fig:test_architecture_network_topology} were created with the exact parameters defined in \autoref{tab:test-architecture-okd}. Both clusters were deployed according to their respective installation documentation (\gls{okd} \cite{InstallingClusterBare}, \gls{ocp} \cite{InstallingClusterBarea}). Since \gls{okd} is the upstream community version of \gls{ocp}, the installation documentation and the steps are mostly identical. The differences between the two include the usage of different \gls{os} images and the need for a Red Hat pull secret to access the enterprise Red Hat resources required for \gls{ocp}. All steps not referenced to other sources can be attributed to these documentations.

For \gls{ocp}, all install-resources required (including the pull secret) can be downloaded after registration from the corresponding tab of Red hat OpenShift Cluster Manager \cite{InstallOpenShiftBare}.

The steps taken during the installation can be summarized as follows:
\begin{enumerate}
	\item On the cl1-okd4-svc node: 
	\begin{enumerate}
		\item The \gls{cli} and installer executables (\gls{okd} \cite{ReleasesOpenshiftOkd}, \gls{ocp} \cite{InstallOpenShiftBare}) were downloaded.
		\item The sample install configuration file \cite{SampleInstallconfigYaml} was adopted according to the planned architecture. A description of all parameters is available at \cite{OpenShiftInstaller}.
		\begin{itemize}
			\item An \gls{ssh} public/private keypair was created and specified in the install configuration, to allow for \gls{ssh} access on all nodes.
			\item For the \gls{ocp} cluster, the Red Hat pull secret was also defined in the install configuration.
		\end{itemize}
		\item The installer was used to generate the cluster manifest from the install configuration file.
		\item The manifest was edited to prohibit the scheduling of Pods on control plane nodes.
		\item Using the \gls{okd} installer the ignition files for the bootstrap, control plane and worker nodes were generated and made available through an Apache virtual host with \acrshort{http}.
	\end{enumerate}
	\item The \acrfull{fcos} and \acrfull{rhcos} bare metal ISO files (containing the kernel, the bootloader and the initramfs) and the raw image files along with their signatures were downloaded. 
	\begin{itemize}
		\item The ISO file was mounted into all nodes.
		\item The raw file with its signature was copied to the same virtual host directory as the ignition files.
		\item Strangely, for the \gls{rhcos} raw file, no signature was provided. 
	\end{itemize}
	\item The nodes require specific kernel parameters at the first boot which specify the required files on the Apache virtual host as well as the install disk. The required parameters can also be specified on the booted live system using the \texttt{openshift-install} command.
	\begin{itemize}
		\item In its current version, the \gls{okd} documentation does not highlight all required kernel parameters, which can be found in section 4.2.12.1 step 6 of the \gls{ocp} documentation \cite{12CreatingRed}.
	\end{itemize}
	\item To try both methods, for \gls{okd} the parameters were specified in the bootloader, while for \gls{ocp} the parameters were specified in the live-system using the \texttt{openshift-install} command.
	\begin{itemize}
		\item For the bootloader method, the required kernel parameters were specified in the boot menu (interrupt with \texttt{E}) and the nodes started the install and cluster bootstrap process.
		\item For the live installer method, the same parameters were defined as parameters of the \texttt{openshift-install} command. After the installer finished on each node, they had to be manually rebooted to enter the cluster bootstrap process.
	\end{itemize}
	\item After the cluster setup finished, the bootstrap node was stopped and removed from the load balancer configuration.
	\item Default login credentials for the dashboard are provided in the install logs and in the \texttt{auth/} directory generated by the installer, as described in the documentation \cite{AccessingWebConsole}.
\end{enumerate}

\begin{table}[h]
	\centering
%	\resizebox{\textwidth}{!}{%
%		\begin{tabular}{@{}lllllll@{}}
	\begin{scriptsize}
	\begin{tabular}{@{}p{0.15\textwidth}p{0.13\textwidth}p{0.22\textwidth}p{0.18\textwidth}p{0.045\textwidth}p{0.06\textwidth}p{0.065\textwidth}@{}}
			\toprule
			\textbf{Name} \mbox{(*.\textit{<cluster>}.cs.lan)} & \textbf{\acrshort{ip}-address} & \textbf{Roles} & \textbf{\acrshort{os}} & \textbf{\acrshort{cpu} cores} & \textbf{Memory} (GiB) & \textbf{Storage} (GiB) \\ \midrule
			cl1-\textit{<dist>}-fw & 172.30.1.1/24, external \acrshort{ip} & Firewall, \acrshort{dhcp}, \acrshort{nat}, reverse-proxy. & FreeBSD (pfSense) & 1 & 1 & 8 \\
			cl1-\textit{<dist>}-svc & 172.30.1.250/24 & \acrshort{dns}, load-balancer, \acrshort{nfs}, \acrshort{http} server. & CentOS 7 (with GNOME) & 2 & 3 & 100 \\
			cl1-\textit{<dist>}-bs & 172.30.1.25/24 & Kubernetes bootstrap node. & \gls{rhcos} 33 / \gls{rhcos} 4.7.13 & 4 & 16 & 120 \\
			cl1-\textit{<dist>}-cp-n1 & 172.30.1.20/24 & Kubernetes control plane node 1. & \gls{rhcos} 33 / \gls{rhcos} 4.7.13 & 4 & 16 & 120 \\
			cl1-\textit{<dist>}-cp-n2 & 172.30.1.21/24 & Kubernetes control plane node 2. & \gls{rhcos} 33 / \gls{rhcos} 4.7.13 & 4 & 16 & 120 \\
			cl1-\textit{<dist>}-cp-n3 & 172.30.1.22/24 & Kubernetes control plane node 3. & \gls{rhcos} 33 / \gls{rhcos} 4.7.13 & 4 & 16 & 120 \\
			cl1-\textit{<dist>}-work-n1 & 172.30.1.23/24 & Kubernetes worker node 1. & \gls{rhcos} 33 / \gls{rhcos} 4.7.13 & 4 & 16 & 120 \\
			cl1-\textit{<dist>}-work-n2 & 172.30.1.24/24 & Kubernetes worker node 2. & \gls{rhcos} 33 / \gls{rhcos} 4.7.13 & 4 & 16 & 120 \\ 
			\bottomrule
		\end{tabular}%
	\end{scriptsize}
%	}
	
	\caption*{
		\begin{footnotesize}
		%\resizebox{\textwidth}{!}{%
			% Table note table, bit hacky but it works.
			\begin{tabular}{l}
				%\toprule
				\textbf{\textit{<cluster>}}: Cluster name as part of the subdomain. ``okd'' for \gls{okd} and ``ocp'' for \gls{ocp}.\\
				\textbf{\textit{<dist>}}: Distribution name as part of hostname. ``okd4'' for \gls{okd} and ``ocp4'' for \gls{ocp}.\\
				%\bottomrule
			\end{tabular}
		%}
		\end{footnotesize}
	}
	
	\caption[Components of the OKD / OCP test architecture.]{Components of the \gls{okd} / \gls{ocp} test architecture, including Kubernetes nodes and servers for required services. Further specification of used software can be found in \autoref{tab:software-versions}.}
	\label{tab:test-architecture-okd}
\end{table}

\subsection{The Rancher Installation}
\label{subsec:test_cluster_methods_rancher_installation}

% Before starting the installation of the cluster, firstly the firewall and the service node were configured as described in \autoref{subsec:test_cluster_methods_firewall_configuration} for the firewall and in \autoref{subsec:test_cluster_methods_service_node_configuration} for the service node.

Similarly to the previous two clusters, an instance of the test architecture defined in \autoref{fig:test_architecture_network_topology} was created with the exact parameters defined in \autoref{tab:test-architecture-rke}. Apart from the common adjustments like adjusting the \gls{dns} server and load balancer configurations, Docker was additionally installed on the service node on this architecture.

When it comes to Rancher and \gls{rke}, the lines and responsibilities of each might not be clear at first. \gls{rke} is an installer like kubeadm, which can deploy and manage Kubernetes clusters running on Docker containers \cite{RKEKubernetesInstallation}. Rancher on the other hand is the Kubernetes distribution, which can be deployed using \gls{rke}, \gls{rke}2 or K3s \cite{DonHaveKubernetes}. In this installation Rancher was deployed using the installer functionality provided by the Rancher dashboard, which under the hood uses \gls{rke} for the deployment but provides a more streamlined installation process. When deploying the cluster using \gls{rke} directly, the installation instructions cover two pages of detailed documentation \cite{SettingHighavailabilityRKE}, while the dashboard installer requires only brief documentation \cite{LaunchingKubernetesExisting}. Despite the significant difference in documentation length, both ways of installation provide the same range of possibilities when deploying a Rancher Kubernetes cluster.

To choose the versions of software components that are guaranteed to be compatible, the ``Installing Rancher v2.5.7 on RKE'' section of the Rancher support matrix was consulted \cite{RancherLabsSupport}.

The steps taken during the installation can be summarized as follows:
\begin{enumerate}
	\item On all nodes except the firewall, the most recent version of Docker supported by Rancher (see: support matrix \cite{RancherLabsSupport}) was installed using the Docker install scripts provided by Rancher \cite{RancherDockerInstall2021}.
	\begin{itemize}
		\item Using a script provided by Docker \cite{PostinstallationStepsLinux2021}, all nodes were tested for the availability of required kernel modules.
	\end{itemize}
	\item The required version of the Rancher Docker container was downloaded and started on the service node to serve as a management and installer node.
	\item In the Rancher dashboard, following steps were executed:
	\begin{enumerate}
		\item Admin password was set, and the required install option was chosen (Add cluster, Existing nodes).
		\item To clarify the usage of certain cluster parameters, the \gls{rke} cluster configuration reference was consulted \cite{RKEClusterConfiguration}.
		\item Additionally to the defaults, the options project network isolation, restricted \gls{psp}, required supported Docker version, and the authorized endpoint were selected.
		\item It was not required to modify the resulting cluster configuration file but it was saved as a backup.
	\end{enumerate}
	\item The corresponding setup commands were generated in the dashboard and executed on the control plane and worker nodes.
	\item The nodes successfully joined the cluster and set up all components, without the need of manual intervention.
\end{enumerate}

\begin{table}[h]
	\centering
	\begin{scriptsize}
%	\resizebox{\textwidth}{!}{%
		\begin{tabular}{@{}p{0.13\textwidth}p{0.13\textwidth}p{0.21\textwidth}p{0.20\textwidth}p{0.055\textwidth}p{0.06\textwidth}p{0.065\textwidth}@{}}
			\toprule
			\textbf{Name} \mbox{(*.rke.cs.lan)} & \textbf{\acrshort{ip}-address} & \textbf{Roles} & \textbf{\acrshort{os}} & \textbf{\acrshort{cpu} cores} & \textbf{Memory} (GiB) & \textbf{Storage} (GiB) \\ \midrule
			cl1-rke-fw & 172.30.1.1/24, external \acrshort{ip} & Firewall, \acrshort{dhcp}, \acrshort{nat}, reverse-proxy. & FreeBSD (pfSense) & 1 & 1 & 8 \\
			cl1-rke-svc & 172.30.1.250/24 & \acrshort{dns}, load-balancer, \acrshort{nfs}, \acrshort{http} server, Rancher management. & CentOS 7 (with GNOME) & 4 & 8 & 100 \\
			%cl1-rke-bs & 172.30.1.25/24 & Kubernetes bootstrap node. & Ubuntu Server 20.04& 4 & 16 & 120 \\
			cl1-rke-cp-n1 & 172.30.1.20/24 & Kubernetes control plane node 1. & Ubuntu Server 20.04& 2 & 8 & 50 \\
			cl1-rke-cp-n2 & 172.30.1.21/24 & Kubernetes control plane node 2. & Ubuntu Server 20.04& 2 & 8 & 50 \\
			cl1-rke-cp-n3 & 172.30.1.22/24 & Kubernetes control plane node 3. & Ubuntu Server 20.04& 2 & 8 & 50 \\
			cl1-rke-work-n1 & 172.30.1.23/24 & Kubernetes worker node 1. & Ubuntu Server 20.04& 2 & 8 & 50 \\
			cl1-rke-work-n2 & 172.30.1.24/24 & Kubernetes worker node 2. & Ubuntu Server 20.04& 2 & 8 & 50 \\ 
			\bottomrule
		\end{tabular}%
	\end{scriptsize}
%	}
	
	\caption[Components of the Rancher test architecture.]{Components of the Rancher test architecture, including Kubernetes nodes and servers for required services. Further specification of used software can be found in \autoref{tab:software-versions}.}
	\label{tab:test-architecture-rke}
\end{table}

\subsection{Evaluation Criteria}
\label{subsec:test_cluster_evaluation_criteria}

The primary aim of the future faculty Kubernetes cluster is to provide a platform for hosting research and student projects in a self-service manner. Currently many projects are covered by providing \gls{lxc} containers or \gls{kvm} \acrshort{vm}s on the faculty Proxmox cluster. Moving some of the current and future projects, especially multi-container projects with special networking needs, to a Kubernetes cluster is expected to reduce the workload of the system administration staff. The primary aim of adapting Kubernetes is to provide isolated environments for students and faculty staff where they can deploy their projects in a mostly self-service manner using a web-dashboard.

Based on feedback from different stakeholders, two main groups of requirements were determined.

\subsubsection{System and Operational Aspects}
\label{subsubsec:test_cluster_system_operation_aspects}

\begin{description}
	\item[Dashboard usability:] User accounts with specific restrictions should be provided to faculty staff and students for access to the Kubernetes dashboard and the Kubernetes \gls{api}. The wish to create and operate the architecture of projects in a self-service manner ("without having to rely on an operations team") was also repeatedly signalized by the faculty staff. It is assumed that a significant percentage of future users do not have the required know-how to set up and operate the microservice architectures required for the projects by relying solely on command line tools such as kubectl. Therefore a Dashboard with great usability allowing to perform a wide range of cluster operations in a self-service manner is of great importance. For all following usecases, the provisioning of corresponding dashboard functionality will be an important aspect. 
	
	\item[Maintainability:] The aim of providing a simplified self-service solution for hosting faculty projects should not come at the cost of a disproportional increase of maintenance workload for the system administration staff. The Kubernetes distribution chosen for the faculty Kubernetes cluster should have reasonable maintenance needs and should be reasonably robust. Regular maintenance tasks such as upgrading the cluster to a new Kubernetes version, backing up cluster configuration (etcd data) or restarting a node for maintenance should come with as few complications as possible. The recovering of the cluster from unexpected incidents such as an abrupt restart of the server where the Kubernetes node is run or a failure of a Kubernetes node should be possible with reasonable effort. A dashboard with great usability and a wide range of functionality is a significant requirement for the system administration staff too. Although the system administration staff is experienced in the usage of different command line tools, it can still benefit greatly from having a well-designed cluster dashboard. Having the cluster management, cluster monitoring and other tooling integrated into the dashboard eliminates the additional workload associated with implementing and maintaining it by the system administration staff itself.
	
	\item[Logging, monitoring and alerting:] The Kubernetes cluster should provide tools for logging, monitoring and alerting as part of the dashboard. The more functionality is provided the better.
	
	\item[Cluster scalability:] Although the faculty Kubernetes cluster is not planned to be scaled frequently and automatically based on workload, a certain degree of scalability is still required. In cases where a university course with many students would require a significant amount of resources on the cluster, it should be possible to add additional nodes to the cluster with reasonable effort. Similarly, when the demand for resources decreases for a longer time, for example after a course has ended, it should be possible to downscale the cluster with reasonable effort. The more functionality is provided for this in the dashboard, the better.
	
	\item[Security:] The Kubernetes cluster should provide reasonable defaults to prevent rouge or misconfigured containers from damaging the cluster.
	\begin{description}
		\item[Authentication and identity provision:] User authentication and identity provision using user data from the university \gls{ldap} directory is a must. The more functionality is provided for this in the dashboard, the better.	
		
		\item[Authorization and resource utilization:] The support to set up a granular authorization for example based on \gls{rbac} combined with resource usage restrictions is an important requirement. To properly isolate the environments of different projects from each other and to prevent over-utilization of resources is a must.
	\end{description}

\end{description}

\subsubsection{Application Aspects}
\label{subsubsec:test_cluster_application_aspects}

Whether the cluster deployed with the given Kubernetes distribution can support the needs of future applications that will be running on the cluster will be determined using multiple real-world projects and requirements.

\begin{description}
	\item[Private container registry:] Given the self-service usage of the future cluster, users should be able to upload custom container images into a local container registry. The private container registry should also be able to cache container images downloaded from other image registries to reduce network load.
	
	\item[Document generating application:] A slide crafting application will be used for testing the cluster functionality under common \gls{cicd} workloads and tasks. The slide crafting application provided as a Docker container is used to compile markdown files into \gls{pdf} and \TeX{} files and is available on GitHub \cite{kienbockSlideCrafting2021}. Combining this application with a GitLab instance installed using the official GitLab Cloud Native Helm Chart \cite{GitLabCloudNative}, a toolchain should be created where a commit to the GitLab repository can spin-up the slide crafting Docker container to build the new slides. This usecase can test the Helm chart integration as well as the behavior of the Kubernetes cluster under everyday \gls{cicd} workloads. The original documentation for deploying the application in a \gls{vm} on top of Docker is provided in \autoref{ch:sliderunner_documentation}.
	
	\item[Student project:] In this usecase identical project environments should be deployed for all students of a planned university course.
	\begin{itemize}
		\item Students should be able to login to the dashboard using their \gls{ldap} credentials.
		\item Identical environments should be deployed for each student by the cluster administrator.
		\item All containers should be preconfigured with the required base configuration and unique credentials for each student.
		\item Students should have the minimum rights required in the environment and in the cluster.
		\item Network isolation and quotas on resource consumption should be applied. 
	\end{itemize}
\end{description}

\subsubsection{Regarding Cluster Performance}
\label{subsubsec:test_cluster_not_significant_aspects}

The raw performance of the analyzed Kubernetes distribution was determined to be of secondary importance. The cluster will host only non-commercial and non-time-critical research projects where some difference in execution time is generally acceptable.