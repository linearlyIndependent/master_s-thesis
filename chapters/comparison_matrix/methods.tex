\section{Methods}
\label{sec:comparison_matrix_methods}
This paper aims to provide a filtered list of Kubernetes distributions suitable for the planned cluster and the selection of the most suitable Kubernetes distribution from the narrowed down list.

\subsection{Selection Criteria for Kubernetes Distributions}
\label{subsec:selection_criteria}

Kubernetes distributions are selected from the official ``\acrshort{cncf} Kubernetes Distributions and Platforms'' spreadsheet available on the \acrshort{cncf} Certified Kubernetes homepage \cite{SoftwareConformanceCertifieda}. The classification of Kubernetes projects as distribution, installer or hosted platform is also based on this spreadsheet.

To narrow down the list of possible Kubernetes distributions, following criteria are used:

\begin{description}
	\item[Certified Kubernetes:] The Kubernetes distribution must be part of the \acrshort{cncf}’s Certified Kubernetes Program \cite{SoftwareConformanceCertifieda}. This ensures that the chosen distribution is interoperable with plain Kubernetes and other certified distributions and ensures yearly updates to the latest Kubernetes version.
		
	\item[Project type:] The chosen project should be classified as a Kubernetes distribution according to the \acrshort{cncf}’s Certified Kubernetes Program \cite{SoftwareConformanceCertifieda}. Hosted Kubernetes platforms and installers are excluded from the comparison.
	
	\item[Up-to-date certification:] The newest Kubernetes version at the time of writing is 1.20. The latest \gls{cncf} certified release of the Kubernetes distribution cannot be older than the previous minor version (1.19).
	
	\item[Setup:] The Kubernetes distribution must provide a setup for setting up a Kubernetes cluster with that Kubernetes distribution. Kubernetes distributions which can only be installed on top of an existing Kubernetes cluster are excluded from the comparison.
	
	\item[Hosting:] The Kubernetes distribution should be available as a self-hosted, on-premise solution which can be installed on a private cluster using \gls{lxc} containers or \gls{qemu}/\gls{kvm} \glspl{vm}. Considering only raw performance, a bare-metal installation of Kubernetes, where each node is run on a dedicated physical server would be the best solution. Since the faculty does not have the hardware resources required for multiple bare-metal Kubernetes clusters, the Kubernetes clusters should be deployed on top of containers or \glspl{vm}. Although this nested approach might lower the overall performance of a cluster, it also offers enhanced scalability since nodes can be deployed quickly without the need to acquire new hardware.
	
	\item[Niche products:] Kubernetes distributions aimed at a niche market (mainly products aimed at a national market) or special use case are excluded from the comparison. Distributions aimed at a niche market mainly include distributions aimed at the Chinese or Korean market. Distributions with a special usecase include distributions aimed at the telecommunications sector or low-resource \gls{iot} applications.
	
	\item[Maturity:] The Kubernetes distribution should have stable releases. Work-in-progress distributions and distributions which did not reach the first stable release are excluded from the comparison.
	
	\item[Price and licensing:] The Kubernetes distribution must provide a freely usable version, community version or similar which can be used without licensing or subscription fees. The planned cluster will serve a single faculty, which limits the financial possibilities of the project. The availability of an additional paid and enterprise supported version which could be acquired in case of long-term satisfaction is an advantage.
	
\end{description}

All entries in the certified Kubernetes distributions spreadsheet will be evaluated according to the above criteria. Entries which do not comply with one or more criterion will be disqualified and excluded from further analysis. For each excluded Kubernetes solution, the specific criterion will be noted according to which it was disqualified. This procedure might be overly restrictive in some cases, but given that the spreadsheet contained 141 certified Kubernetes solutions at the time of writing, it is required so that the feature comparison remains within the possibilities of this paper. 


\subsection{Evaluation Criteria for Selected Kubernetes Distributions}
The narrowed-down list of Kubernetes distributions will be evaluated according to criteria extracted from the findings of \autoref{sec:trends_soa} and the results will be summarized in a feature comparison matrix. 

The following criteria will provide the rows, while the selected Kubernetes distributions will provide the columns of the matrix. Where possible, each cell will provide a compact summary or listing and in cases where this is not possible, a symbol will indicate whether the criterion is satisfied. ``\colorbox{lgreen}{ + }'' will indicate the satisfaction of the criterion, while ``\colorbox{lyellow}{ \textasciitilde }'' will indicate the partial satisfaction and ``\colorbox{lorange}{ - }'' the dissatisfaction of the criterion. \colorbox{lblue}{<listing>} will be used where options provided by different distributions are presented for comparison.

\begin{description}
		
	\item[Strong market position:] Whether the distribution was found to be a market leader or an outstanding Kubernetes distribution by the market analysis in \autoref{subsec:distribution_trends}.
	
	%\item[Number of \gls{cncf} certifications:] The total number of \gls{cncf} certifications for the Kubernetes Distribution. A higher number and a continuous certification for consecutive Kubernetes version shows that the distribution stays up to date and compatible with the latest Kubernetes versions.

	\item[Available high-level runtimes:] Due to the \gls{cri} interface and the dockershim Kubernetes supports containerd, CRI-O and Docker as high-level runtimes, but Kubernetes distributions tend to restrict the range of available hig-level runtimes. %Due to the \gls{cri}, Kubernetes should behave the same with all high-level runtimes, but having Docker as the only available runtime will be considered as a drawback.

	\item[\gls{cni} plugins:] Kubernetes supports all container networking plugins which implement the \gls{cni}. As with the previous criteria, Kubernetes distributions impose restrictions on which \gls{cni} plugins they officially support.

	\item[Persistent storages:] Depending on the chosen Kubernetes distribution, containers can use different storage technologies for their persistent storage needs, including \gls{nfs}, \gls{iscsi} or different cloud-vendor-specific storage technologies.

	\item[Cluster upgrades and rollbacks:] Upgrading a Kubernetes cluster to a new version requires the draining (evicting all deployments from the node) and upgrading of all nodes one at a time, and the adjustment of the configurations according to the changelogs \cite{UpgradeCluster}. Kubernetes distributions may provide simplified cluster upgrade features or rollback features in case of a failed upgrade.
	
	\item[Helm integration:] Helm is the de-facto standard of Kubernetes application packaging, as described in \autoref{subsec:devops_tools_trends}. Helm 3 simplified the integration with Kubernetes by getting rid of the server side Helm component and instead building on native Kubernetes components. Still, Kubernetes distributions might provide enhanced Helm integration and simplified interaction with Helm packages and applications.
	
	\item[Privileged containers:] Measures taken by the Kubernetes distribution against running privileged containers and against the privilege escalation of containers.
	
	\item[Vulnerability scanning:] Host-\gls{os} and container security scanning tools provided by the distribution.
	
	%\item[\gls{cicd} integration:] Provided \gls{cicd} tools and integration by the distribution.
	
	\item[Metrics and monitoring:] Metrics and monitoring tools provided by the distribution.
	
	\item[Dashboard, authentication, identity providers:] Although Kubernetes provides the Kubernetes Dashboard as a standard dashboard, this does not come with sophisticated user management features or out-of-the-box support for different \gls{sso} and authentication systems or identity providers \cite{AccessingKubernetesDashboard}. Kubernetes distributions therefore often provide their own dashboards with extended functionalities and integration with authentication systems and identity providers. 
	
	\item[Support:] Enterprise support subscriptions available for the given Kubernetes distribution. For the long-term maintenance and expansion of the final Kubernetes cluster, the availability of enterprise support is a great benefit.
	
\end{description}
