\section{Results}

\subsection{Kubernetes Distributions Excluded from Comparison}

The range of available Kubernetes solutions was narrowed down to four distributions according to the methodology defined in \autoref{subsec:selection_criteria}. The list of all analyzed Kubernetes solutions which contains also the reasons for excluding certain distributions is available in \autoref{ch:cncf_distributions_platforms}. A visualization of the percentage of Kubernetes products that were disqualified according to a given criteria is given in \autoref{fig:kubernetes_product_disqualified}.

\begin{figure}[h!]
	\centering
	\begin{tikzpicture}
		\pgfplotsset{
			every axis legend/.append style={
				%at={(0.63,0.63)},
				at={(1.5,0.35)},
				anchor=south
			},
		}
		
		\begin{axis}[
			axis lines = left,
			x axis line style = -,
			y axis line style = {-latex},
			legend style={font=\small},
			every axis plot/.append style={ultra thick},
			%legend pos=outer north east,legend
			%style={at={(40.5,-0.1)},anchor=north},
			%xlabel = $n_f$,
			%scale only axis,
			width=0.5\linewidth,
			height=0.43\linewidth,
			xlabel = ~,
			%axis x line=none,
			ylabel = {Disqualified products (\%)},
			ymajorgrids=true,
			ytick distance=10,
			grid style=dashed,
			%width=0.8\linewidth,
			xtick=\empty,
			%major x tick style = transparent,
			%enlarge x limits=0.25,
			%axis x line=none,
			xmin=0,
			xmax=5,
			ymin=0,
			ymax=60,
			]
			\addplot+ [
			ybar, fill=red, red, mark=square*, mark options={scale=2, black, fill=white},% mark=none,
			] coordinates {
				(1,57)
			};
			\addlegendentry{Installer or cloud-only: $57\%$}
			
			\addplot+ [
			ybar, fill=red, blue, mark=*, mark options={scale=2, black, fill=white},% mark=none,
			] coordinates {
				(2,24)
			};
			\addlegendentry{Niche product: $24\%$}
			
			
			\addplot+ [
			ybar, fill=red, green, mark=triangle*, mark options={scale=2, black, fill=white},% mark=none,
			] coordinates {
				(3,13)
			};
			\addlegendentry{Pricing or licensing: $13\%$}
			
			\addplot+ [
			ybar, fill=red, orange, mark=diamond*, mark options={scale=2, black, fill=white},% mark=none,
			] coordinates {
				(4,11)
			};
			\addlegendentry{Other: $11\%$}
		\end{axis}
	\end{tikzpicture}
	\caption{Percentages of different Kubernetes products being disqualified due to a specific issue.}
	\label{fig:kubernetes_product_disqualified}
\end{figure}

The largest group of disqualified Kubernetes solutions, which make up 57\% of all disqualified solutions, are installers and hosted/cloud-only distributions. Installers generally do not add significant additional value to standard Kubernetes except for the simplified installation process and cloud-only distributions are not suitable for private, on-premise deployment. 

Another significant group of disqualified Kubernetes distributions are niche products, making up around 24\% of all disqualified solutions. Most often, they are distributions targeting a national market, for example Chinaunicorn Kubernetes Engine for the Chinese market, or the TmaxA\&C HyperCloud for the South-Korean market. There are also niche products targeting specific technical usecases like Ericssons's Cloud Container Distribution specialized for the telecommunication sector, or Rancher's K3s and Canonical's MicroK8s aimed at low-resource/\gls{iot} usecases.

The last significant group of disqualified distributions, which makes up around 13\% of disqualified distributions, contains distributions which are excluded due to pricing and licensing reasons. These distributions do not provide a freely-available option or cannot be deployed on a private, self-hosted infrastructure. These distributions were excluded to ensure, that the practical possibilities of the following project will not be limited by timely or financial constraints. 

Other criteria were only analyzed if a Kubernetes solution was not disqualified by the previous three criteria. Due to this, other criteria such as the immaturity, the abandoned state or the missing \gls{cncf} certifications for the last two minor Kubernetes versions make up less then 11\% of all disqualifications.


\subsection{Kubernetes Distributions Selected for Comparison}
Following Kubernetes distributions were found to fulfill all criteria defined in \autoref{subsec:selection_criteria} and therefore qualify for the feature comparison matrix evaluation:

\begin{itemize}
	%\item Standard Kubernetes installed with kubeadm, an official Kubernetes installer (see: kubeadm documentation \cite{KubernetesDocumentationKubeadm}).
	\item \gls{ocp} \cite{RedHatOpenShifta} or \acrshort{okd} \cite{OKDCommunityDistribution}: \acrshort{okd}, the upstream community Kubernetes distribution of \gls{ocp} is a viable option for evaluation. In case that enterprise support is required, the cluster can be migrated to \gls{ocp}. If \gls{okd} is found to be a viable option, \gls{ocp} could be analyzed alongside it in the practical analysis (using a 60 day trial access) to determine the practical differences between the two distributions.
	\item Rancher \cite{Rancher}: Paid Kubernetes distribution, with available free community usage without support \cite{RancherKubernetesEngine}.
	\item Charmed Kubernetes \cite{CharmedKubernetesMulticloud}: Free to use Kubernetes distribution by Canonical with optional paid enterprise support.
	\item Kublr \cite{EnterpriseReadyKubernetesKublr}: Free to use Kubernetes distribution with optional paid enterprise support.
\end{itemize}

It has to be noted, that openSUSE Kubic was not analyzed although it met all requirements defined in \autoref{subsec:selection_criteria}. openSUSE Kubic was not analyzed as its future is uncertain after SUSE's acquisition of Rancher, although new versions of Kubic are released regularly even after the acquisition.

The complete list of all evaluated Kubernetes solutions, and the reasons for excluding them from further analysis is provided in \autoref{ch:cncf_distributions_platforms}.


\subsection{Analysis of Kubernetes Distributions Using a Feature Matrix}
\label{subsec:feature_matrix}

The information required for the comparison was gathered primarily from the documentation and in some cases from the official websites, repositories or blogs of the distributions. Apart from some aspects of Kublr (which will be addressed later in this section), the required information could be successfully collected.

Since \gls{okd} is the upstream version of \gls{ocp}, wherever \gls{okd} is mentioned, the same applies to \gls{ocp} too. \gls{ocp} will only be referenced in cases where something applies to \gls{ocp} only. Since the documentations of both distributions are generated from the same base, they are mostly identical. Therefore, the \gls{ocp} documentation will only be referenced if required.


\subsubsection{Analysis of Technical Criteria}
A wide range of technical parameters have to be considered when choosing a Kubernetes distribution and deploying a Kubernetes cluster. Starting with the possible high-level runtimes, throughout the \gls{cni} networking plugins until the used authentication system or identity provider, a wide range of options are available. 

Although Kubernetes allows the usage of any high-level runtime implementing the \gls{cri}-\gls{api}, Kubernetes distributions tend to restrict the range of available options. \gls{okd} allows only \gls{cri-o} as a high-level runtime \cite{ControlPlaneArchitecture}, Rancher allows only Docker \cite{HowThisDifferent}, while Charmed Kubernetes allows either Docker or containerd, the later being the default option \cite{ContainerRuntimes}. Considering the supported container runtimes, the Kublr documentation does not provide any information, but based on information from an official Kublr webinar \cite{Kubernetes101Intro2021}, Docker is supported.

In terms of persistent container storage options, \gls{okd}, Charmed Kubernetes and Rancher all support the most common storage technologies including local host storage, \gls{nfs}, \gls{iscsi}, VMware vSphere and a wide range of cloud vendor storages. \gls{okd} and Charmed Kubernetes provide a considerably larger selection of supported storage options than Rancher. Similarly to the previous aspect, the Kublr documentation does not provide any information regarding the supported storage options, apart from the support of certain cloud vendor storages \cite{Types}. For the complete list of supported storage technologies as well as references to the corresponding parts of the documentations please refer to \autoref{tab:feature-matrix}.

Regarding \gls{cni} cluster networking plugins, Rancher, Charmed Kubernetes and Kublr support all commonly used plugins, including Canal \cite{Canal2021}, Flannel \cite{Flannel2021}, Calico \cite{Calico} and Weave \cite{WeaveNet2021}. Canal was a project aimed at combining Flannel and Calico into a single plugin, and although it has been discontinued, the name Canal is still used to refer the combined usage of Flannel and Calico as described in \cite{Canal2021}. A comparison of these plugins is available on the Rancher Blog \cite{ellingwoodComparingKubernetesCNI2019}. \gls{okd} takes a different approach by not supporting any of the previously noted plugins and focusing on RedHat’s self-developed OpenShift \acrshort{sdn} plugin \cite{OpenShiftSDNDefault}.

As the default networking plugin, \gls{okd} uses the OpenShift \acrshort{sdn}, Charmed Kubernetes uses Flannel and Rancher and Kublr uses Canal. \gls{okd} and Charmed Kubernetes also support Multus \cite{MultusCNI2021}, which allows to define multiple additional networks with different \gls{cni} plugins that can be attached to given Pods. Multus uses a default \gls{cni} plugin for the default cluster network (which also gets attached to every Pod), and can use any \gls{cni} plugin to define additional networks \cite{CNIConfigurations}. Charmed Kubernetes does not restrict the allowed \gls{cni} plugins which can be used by Multus \cite{CNIMultus}, while \gls{okd} uses the \gls{ocp} \acrshort{sdn} as default plugin and allows the usage of SR-IOV, ipvlan, macvlan, host-device and Linux bridge devices for additional networks \cite{UnderstandingMultipleNetworksa}.

SR-IOV \cite{SRIOVNetworkDevice2021}, which allows to share \gls{pcie}-networking hardware and attach them to containers, is supported by Charmed Kubernetes and \gls{okd}. For a complete list of \gls{cni} networking plugins for each Kubernetes distribution, as well as references to the corresponding parts of the documentations please refer to \autoref{tab:feature-matrix}.

Kubernetes distributions tend to provide their own dashboards combined with enhanced authentication capabilities and integration possibilities with identity providers. For authentication, \gls{okd} provides an integrated \gls{oauth} client \cite{UnderstandingAuthenticationAuthentication}, Rancher provides its own authentication proxy \cite{Authentication}, Charmed Kubernetes provides a Webhook authenticator \cite{ConfiguringAuthorisationAuthentication} and Kublr provides its own authentication proxy \cite{KubectlOIDCAuthentication}. As for identity provision, both \gls{okd} and Rancher provide a wide range of options, including \gls{ldap}, Active Directory, GitHub and Google. Charmed Kubernetes provides only \gls{ldap} and \gls{aws} and Kublr provides OpenID Connect and Keycloak. For a complete list of supported authenticators and identity providers, as well as references to the corresponding parts of the documentations please refer to \autoref{tab:feature-matrix}.

An integration of Helm into the \gls{cli} and the dashboard of the distribution is provided only by \gls{okd} \cite{HelmChartsRancher} and Rancher. %Since Helm is a mostly client-side tool, the missing of Helm integration for Charmed Kubernetes and Kublr is considered only as a partial satisfaction, not a dissatisfaction of this criteria.

To collect metrics and to monitor the Kubernetes cluster, all analyzed distributions provide a similar software stack consisting of Prometheus and Grafana, although it has to be noted that Charmed Kubernetes provides only community supported tools in this area. Additionally, \gls{okd}, Rancher and Kublr also integrate Alertmanager.

Minor Kubernetes releases are published quarterly and each release is supported with patch releases for one year \cite{KubernetesVersionVersion}. Patch releases are published in the first 9 months, one release each month. This fast pace of releases requires the regular updating of Kubernetes clusters to ensure security and continued support. Kubernetes distributions may provide simplified and integrated cluster upgrading functionality to reduce the workload of regular cluster updates. \gls{okd} provides this through a dedicated Kubernetes Operator, combined with the required \gls{cli}-tooling and dashboard functionality. Upgrades between minor and patch releases are available both through the \gls{ocp}-\gls{cli} and over the web-interface \cite{UnderstandingOpenShiftUpdate}. Contrary to the great update support provided by \gls{okd}, the rollback of the cluster to a previous version is not supported. Another distribution providing both \gls{cli} and dashboard support for upgrades and rollbacks is Kublr. Rancher also provides upgrading functionality, with the same feature scope as \gls{okd}. Contrary to \gls{okd}, Kublr also supports the rollback of the cluster to a previous version, but only if a previous update has failed and the cluster entered the corresponding state \cite{KubernetesInPlaceUpgradea}. Charmed Kubernetes does not provide upgrade and restore functionality integrated into the dashboard.

%Charmed Kubernetes takes a similar approach, with the difference that the Kubernetes components are deployed as Juju Charms, the deployment recipes developed by Canonical. Patch releases are automatically updated, while the updates of minor releases and rollbacks are done with the same methodology as for Rancher, only with Juju Charms (see: documentation \cite{Upgrading}). Contrary to \gls{okd}, Kublr also supports the rollback of the cluster to a previous version, but only if a previous update has failed and the cluster entered the corresponding state \cite{KubernetesInPlaceUpgradea}. Charmed Kubernetes does not provide upgrade and restore functionality integrated in the dashboard. If Rancher is deployed using \gls{rke}, it can make use of the design of \gls{rke} for the upgrade and restore functionality.

\subsubsection{Analysis of Security-Relevant Criteria}

Kubernetes is a complex technology which requires well-founded knowledge and expertise to operate properly. As it has been shown in \autoref{sec:trends_soa}, complex technology combined with missing expertise results in a significant number of IT-security incidents. Additionally functionality for vulnerability scanning and enforcing of best-practices can help to reduce the risk of Kubernetes-related security incidents. The type and extent of such functionalities varies greatly between Kubernetes distributions.

One area in which Kubernetes distributions provide additional security functionality is vulnerability scanning. Charmed Kubernetes and Kublr do not provide such functionalities, while \gls{okd} provides the most functionality in this area. 

Firstly, Red Hat offers an official set of regularly curated, scanned and updated (\gls{oci} compliant) \gls{ubi} base images which are available in freely accessible \gls{rpm} package repositories. \gls{ubi} are based on \gls{rhel} and should benefit from the same level of stability, security and release cycle. For more information regarding \gls{ubi} images, refer to the Red Hat Blog’s corresponding article \cite{mccartyIntroducingRedHat2019} and the documentation \cite{SecuringContainerContent}. Secondly, container images in Red Hat’s Quay image registry undergo static vulnerability scans by Clair, Red Hat’s open-source container scanner, as described in the \gls{okd} documentation \cite{SecuringContainerContent}. Thirdly, \gls{okd} also provides the Container Security Kubernetes Operator, which regularly queries the Quay registry for container vulnerability information and reports back to the Kubernetes \gls{api} \cite{SecuringContainerContent}. Vulnerability information can then be accessed through the \gls{okd}-\gls{cli} or through the dashboard.

Rancher does not provide container vulnerability scanning functionalities, but it provides kube-bench \cite{Kubebench2021} and Sonobuoy \cite{Sonobuoy2021} integration \cite{CISScans}. The former allows the running of \gls{cis} Kubernetes cluster security benchmarks, while the later allows amongst others the running of Kubernetes conformance tests.

Another important area, where Kubernetes distributions can provide additional security functionality, is the enforcing of best-practices, amongst others against too permissive privileges, privilege escalation and the mounting of sensitive directories. Except Charmed Kubernetes, all analyzed distributions provide default configurations which aim to enforce such best practices. 

Both Rancher and Kublr provide a restrictive default \gls{psp}, which forbids Pods to be run as root, use certain Linux namespaces, mount sensitive volumes or have the rights for privilege escalation. A detailed description of these restricted \gls{psp}s can be found in the Rancher \cite{CISBenchmarkRancher} and the Kublr documentations \cite{PodSecurityPoliciesa}. \gls{okd} uses \glspl{scc}, which are Red Hat’s solution that predates the Kubernetes \gls{psp} and provides similar functionality. The restricted default \gls{scc} provides similar presets as the restrictive \gls{psp} \cite{ManagingSecurityContext}. As an additional security measure, \gls{okd} also enforces a default seccomp security profile. Finally, Charmed Kubernetes does not provide such functionalities.


\subsubsection{Analysis of Non-Technical Criteria}
When evaluating different products, non-technical criteria such as the market share, market position as well as the provided support are significant factors to determine the potential long-term sustainability of the given product.

The Kubernetes distribution with the largest market share and the best market position was \gls{ocp}/\gls{okd}. \gls{ocp} was found to be one of the three market leading container platforms by reports \cite{bartolettiForresterNewWave2018a,daiForresterWaveMulticloud2020}. Furthermore, it was found to be the fourth most widely used container orchestrator in 2019 by survey \cite{2019ContainerAdoption2019}. Lastly, \gls{ocp} was used by 23\% of respondents in 2019 according to survey \cite{2019ContainerAdoption2019} and by 22\% of respondents in 2020 according to survey \cite{StateKubernetes20202020}.

The distribution with the second largest market share is Rancher, which was used by 11\% of respondents in 2020 according to survey \cite{StateKubernetes20202020}. Similarly to \gls{ocp}, Rancher was also found to be one of the three market leading container platforms in reports \cite{bartolettiForresterNewWave2018a,daiForresterWaveMulticloud2020}.

Charmed Kubernetes was only mentioned once as a noteworthy contender (coming after market leaders and strong performers) in report \cite{daiForresterWaveMulticloud2020}. Kublr was not mentioned in any of the analyzed user surveys and market reports.

Considering the offered enterprise support, all analyzed distributions provide at least a standard and an extended support plan (for \gls{okd} the enterprise supported version is \gls{ocp}). The standard support plan provides support from Mondays to Fridays from 9am to 5pm for all analyzed distributions (except Kublr), but it does not cover all severity levels and means of communication. The extended support plan for these distributions offers 24/7 or Mo-Fr 9am-5pm support with all or additional severity levels and means of communication and faster response times. The support plans are highly flexible and pricing is usually determined individually. Further information regarding support and pricing can be found in \gls{ocp}’s terms of support \cite{ProductionSupportTerms}, in the support datasheet of Charmed Kubernetes \cite{KubernetesEnterpriseDatasheet2020}, Kublr’s pricing information page \cite{PricingFreeNonprod} and Rancher’s terms of support \cite{RancherLabsSupport} and support matrix \cite{SupportMatrixRancher}.


%\begin{landscape}
\begin{table}
	
	%\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
	\centering
	%\ra{1.3}
	%\resizebox{1.5\textwidth}{!}{
	\begin{scriptsize}
	\begin{tabular}{@{}p{0.17\textwidth}p{0.19\textwidth}p{0.19\textwidth}p{0.19\textwidth}p{0.19\textwidth}@{}}%{@{}rllll@{}}%{@{}p{0.48\textwidth}p{0.6\textwidth}p{0.6\textwidth}p{0.6\textwidth}p{0.6\textwidth}@{}}
		%{@{}p{0.15\textwidth}p{0.3\textwidth}p{0.3\textwidth}p{0.3\textwidth}p{0.3\textwidth}@{}}
		\toprule
		 & \textbf{\acrshort{okd} / \gls{ocp}} & \textbf{Rancher (\acrshort{rke})} & \textbf{Charmed Kubernetes} & \textbf{Kublr} \\
		%\cmidrule{2-4} \cmidrule{6-8} \cmidrule{10-12}& $t=0$ & $t=1$ & $t=2$ \\
		\midrule
		
		\textbf{Market share} & \cellcolor{lgreen} 22-23\% (2019 \cite{2019ContainerAdoption2019}, 2020 \cite{StateKubernetes20202020}) & \cellcolor{lgreen} 11\% (2020 \cite{StateKubernetes20202020}) & \cellcolor{lyellow} - & \cellcolor{lyellow} - \\
		
		\textbf{Market position} &\cellcolor{lgreen} Market leader (2018 \cite{bartolettiForresterNewWave2018a}, 2020 \cite{daiForresterWaveMulticloud2020}) &\cellcolor{lgreen} Market leader (2018 \cite{bartolettiForresterNewWave2018a}, 2020 \cite{daiForresterWaveMulticloud2020}) &\cellcolor{lyellow} Noteworthy contender (2020 \cite{daiForresterWaveMulticloud2020}) &\cellcolor{lorange} - \\
		
		\textbf{High-level runtimes} &\cellcolor{lgreen} \textbf{\acrshort{cri-o}} &\cellcolor{lyellow} \textbf{Docker} &\cellcolor{lgreen} \textbf{containerd}, Docker &\cellcolor{lorange} Docker \cite{Kubernetes101Intro2021}, not specified in documentation. \\
		
		\textbf{Persistent storages} &\cellcolor{lblue} hostPath, local volume, NFS, vSphere, iSCSI, OpenStack Manila, flexVolume, Red Hat OpenShift Container Storage, different cloud vendor storages \cite{UnderstandingPersistentStorage}.&\cellcolor{lblue} NFS, vSphere, hostPath, iSCSI, GlusterFS, different cloud vendor storages \cite{KubernetesPersistentStorage}. &\cellcolor{lblue} Local disk, Ceph \cite{Storage}, NFS, vSphere \cite{VSphereIntegratorCharm}, iSCSI \cite{EnterpriseKubernetesDatasheet}, PortWorx, StorageOS, Trident \cite{EnterpriseKubernetesDatasheet}, flexVolume \cite{EnterpriseKubernetesDatasheet}, different cloud vendor storages \cite{EnterpriseKubernetesDatasheet}. & \cellcolor{lorange} Different cloud vendor storages, documentation incomplete \cite{Types}. \\
		
		\textbf{\acrshort{cni} plugins} &\cellcolor{lblue} \textbf{OpenShift \acrshort{sdn}} \cite{OpenShiftSDNDefault}, OVN-Kubernetes \cite{OpenShiftSDNDefault}, Kuryr, Multus, \acrshort{sr-iov} \cite{UnderstandingMultipleNetworksa}. &\cellcolor{lblue} \textbf{Canal}, Flannel, Calico, Weave \cite{ContainerNetworkInterfacea} &\cellcolor{lblue} \textbf{Flannel}, Calico, Canal, Tigera Secure EE, Juniper Contrail \cite{ContrailKubernetesMaster}, Multus, \acrshort{sr-iov} \cite{CNIOverview} &\cellcolor{lblue} \textbf{Canal}, Calico, Flannel, Weave \cite{KublrClusterSpecification} \\
		
		\textbf{Cluster upgrade} &\cellcolor{lgreen} \acrshort{cli} and dashboard \cite{UnderstandingOpenShiftUpdate}.&\cellcolor{lgreen} \acrshort{cli} and dashboard \cite{UpgradingRancherInstalled}. &\cellcolor{lyellow} \acrshort{cli} only, patch releases automatically. \cite{Upgrading} &\cellcolor{lgreen} \acrshort{cli} and dashboard \cite{KubernetesInPlaceUpgradea}.\\
		
		\textbf{Cluster rollback} &\cellcolor{lorange} Not supported. \cite{UnderstandingOpenShiftUpdate}. &\cellcolor{lgreen} \acrshort{cli} and dashboard \cite{RestoringBackupsDocker}. &\cellcolor{lyellow} \acrshort{cli} only. \cite{Upgrading} &\cellcolor{lgreen} \acrshort{cli} and dashboard \cite{KubernetesInPlaceUpgradea}. \\
				
		\textbf{Non-root containers} &\cellcolor{lgreen} Restricted \gls{scc} and seccomp profiles \cite{ManagingSecurityContext}. &\cellcolor{lgreen} Restricted \gls{psp} \cite{CISBenchmarkRancher}. &\cellcolor{lorange} - &\cellcolor{lgreen} Restricted \gls{psp} \cite{PodSecurityPoliciesa}. \\	
			
		\textbf{Vulnerability scanning} &\cellcolor{lgreen} \acrshort{ubi} base images \cite{mccartyIntroducingRedHat2019,SecuringContainerContent}, registries regularly scanned and reported to \acrshort{cli} and dashboard \cite{SecuringContainerContent}. &\cellcolor{lyellow} Cluster security benchmarks and conformance tests \cite{CISScans}. &\cellcolor{lorange} - &\cellcolor{lorange} - \\
		
		\textbf{Helm integration} &\cellcolor{lgreen} Deployment of charts through the \acrshort{cli} and dashboard \cite{GettingStartedHelm}. &\cellcolor{lgreen} Deployment of charts through \acrshort{cli} and dashboard \cite{HelmChartsRancher}. &\cellcolor{lyellow} - &\cellcolor{lyellow} - \\
		
		\textbf{Metrics and monitoring} &\cellcolor{lblue} Alertmanager, Prometheus, Grafana, Thanos, Telemeter \cite{UnderstandingMonitoringStack}. &\cellcolor{lblue} Alertmanager, Grafana, Prometheus \cite{MonitoringAlerting}. &\cellcolor{lyellow} Community integrations: Prometheus, StatsD, Grafana, Nagios, Elasticsearch. &\cellcolor{lblue} Alertmanager, Prometheus and Grafana \cite{Monitoring}. \\
		
		\textbf{Dashboard} &\cellcolor{lgreen} + &\cellcolor{lgreen} + &\cellcolor{lyellow} \textasciitilde{} &\cellcolor{lyellow} \textasciitilde{} \\
		
		\textbf{Authentication} &\cellcolor{lblue} \gls{oauth} \cite{UnderstandingAuthenticationAuthentication}. &\cellcolor{lblue} Local authentication, Rancher authentication proxy \cite{Authentication}. &\cellcolor{lblue} Webhook authenticator \cite{ConfiguringAuthorisationAuthentication}. &\cellcolor{lblue} Authentication proxy \cite{KubectlOIDCAuthentication}. \\
		
		\textbf{Identity providers} &\cellcolor{lblue} \acrshort{ldap}, ActiveDirectory, OpenID Connect, HTPasswd, Keystone, GitHub, GitLab, Google \cite{UnderstandingIdentityProvider}. &\cellcolor{lblue} \acrshort{ldap}, Active Directory, GitHub, FreeIPA, PingIdentity, Keycloak, Okta, Google, Shibboleth \cite{Authentication}. &\cellcolor{lblue} \acrshort{ldap}, \acrshort{aws} \cite{ConfiguringAuthorisationAuthentication}. &\cellcolor{lblue} OpenID Connect, Keycloak \cite{ExternalOpenIDConnect}. \\
		
		\textbf{Support} &\cellcolor{lyellow} \textasciitilde{}: only for \gls{ocp} &\cellcolor{lgreen} + &\cellcolor{lgreen} + &\cellcolor{lgreen} + \\
		
		\bottomrule
	\end{tabular}
	%}

 \caption*{
 	
 	% Table note table, bit hacky but it works.
	\begin{tabular}{l l l}
		%\toprule
		\colorbox{lgreen}{+}: satisfied & \colorbox{lyellow}{\textasciitilde{}}: partly satisfied & \colorbox{lorange}{-}: not satisfied or no information available \\
		\colorbox{lblue}{{\textless}options{\textgreater}}: available options & {\textless}\textbf{default}{\textgreater}: the default option & \\
		%\bottomrule
	\end{tabular}
}
	
	\end{scriptsize}

\caption[The feature comparison matrix.]{The feature comparison matrix, based on the findings of \autoref{subsec:feature_matrix}.}
\label{tab:feature-matrix}
\end{table}
%\end{landscape}

\subsection{Kubernetes Distributions Selected for Practical Evaluation}
\label{subsec:comparison_matrix_results_selection}

Based on the findings of the feature-matrix, \gls{okd}, \gls{ocp} and Rancher (with \gls{rke}) were chosen for the following practical evaluation of Kubernetes distributions. The color coding of the feature matrix might have already given a hint regarding which distributions performed best and in which criteria they excelled during the analysis. Nevertheless, a compact explanation of the reasoning behind choosing these distributions might still be beneficial for the reader.

Firstly the reasoning behind choosing \gls{okd}/\gls{ocp} should be provided. A significant factor for selecting \gls{okd} was its strong and recognized market position, more precisely those of its commercial version, \gls{ocp}. Being repeatedly chosen as one of the market leading solutions since 2018, having over 20\% usage between respondents since 2019 and Red Hat’s positive reputation in the open source community make it a favorable and possibly future-proof candidate. Besides the market share, \gls{okd}/\gls{ocp} provides the most advanced integration of security (\gls{scc}, seccomp profiles, \gls{ubi} images, registry scans, vulnerability reporting, authentication and identity provision) and non-security tooling (Helm, monitoring tools, cluster upgrades). The provided support for a wide range of storage technologies and \gls{cni} plugins might also be beneficial on the long term. Despite all the advantages, the two drawbacks of \gls{okd}, which are the missing support for rolling back to previous Kubernetes versions and the lack of enterprise support need to be accounted for in the long term. In case of a long-term enterprise usage, a migration to \gls{ocp} would certainly be beneficial if the financial resources are available.

Rancher also showed the same strong market position as \gls{okd}/\gls{ocp} by being chosen as one of the other market leaders since 2018 and by having a considerable 11\% usage by respondents in 2020 and great popularity in the DevOps community. Similarly to \gls{okd}, it was found to provide good dashboard functionalities and a good integration of different tools, although it lacks the dashboard-integrated Kubernetes upgrade functionality and the advanced security tooling. The usage of Docker as a high-level runtime would be a long-term drawback due to the dockershim deprecation, but since \gls{rke}2 (which uses containerd) is already available for experimental usage this should not be a significant drawback. The possibility to subscribe for enterprise support without the need to migrate to a different distribution is a significant advantage in comparison to \gls{okd}.

Charmed Kubernetes and Kublr did not manage to qualify for further analysis. Both distributions fell short in terms of security relevant tooling (vulnerability scanning or the enforcement of best practices) as well as overall dashboard functionality. Furthermore, both distributions lacked a strong market presence. Lastly, Kublr was also found to have a basic and in some cases incomplete documentation leaving the impression of a possibly immature distribution.