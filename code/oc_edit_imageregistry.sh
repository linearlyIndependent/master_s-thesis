#!/bin/bash

oc edit configs.imageregistry.operator.openshift.io

# Set the management state and the storage field:
#
#spec:
# ...
#  managementState: Managed
#...
#  storage:
#    pvc:
#      claim:
