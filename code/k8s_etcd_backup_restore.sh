# Install the corresponding etcdctl release on all Kubernetes master nodes!

# Backup etcd on one node only
[root@cl1-k8s-cp-n1 ~]# etcdctl --endpoints=https://127.0.0.1:2379 --cacert=/etc/kubernetes/pki/etcd/ca.crt \
    --cert=/etc/kubernetes/pki/etcd/healthcheck-client.crt \
    --key=/etc/kubernetes/pki/etcd/healthcheck-client.key snapshot save backup-dir/snapshot.db

# Verify the backup
[root@cl1-k8s-cp-n1 ~]# etcdctl --write-out=table snapshot status backup/snapshot.db

# Backup also the cluster private keys to be sure
[root@cl1-k8s-cp-n1 ~]# cp -r /etc/kubernetes/pki backup-dir/
# Copy this backup directory to all Kubernetes master nodes

#backup the static pod manifests on all nodes
[root@cl1-k8s-cp-n1 ~]# cp -r /etc/kubernetes/manifests backup-dir/

# Copy the snapshot.db to all master nodes.

# Retrieve the etcd launch command from ...
# ... the etcd pod manifest /etc/kubernetes/manifests/etcd.yaml
# ... or using kubectl describe for the etcd pod 
# Based on this, restore the etcd backup on each node accordingly:

[root@cl1-k8s-cp-n1 ~]# etcdctl snapshot restore backup-dir/snapshot.db --data-dir /var/lib/etcd-restored-19.05.21 \
  --endpoints=https://127.0.0.1:2379,https://172.30.1.20:2379 \
  --cacert=/etc/kubernetes/pki/etcd/ca.crt \
  --cert=/etc/kubernetes/pki/etcd/server.crt \
  --key=/etc/kubernetes/pki/etcd/server.key \
  --name=cl1-k8s-cp-n1 \
  --initial-cluster=cl1-k8s-cp-n3=https://172.30.1.22:2380, cl1-k8s-cp-n1=https://172.30.1.20:2380,cl1-k8s-cp-n2=https://172.30.1.21 :2380 \
  --initial-advertise-peer-urls=https://172.30.1.20:2380

[root@cl1-k8s-cp-n2 ~]# etcdctl snapshot restore backup-dir/snapshot.db --data-dir /var/lib/etcd-restored-19.05.21 \
  --endpoints=https://127.0.0.1:2379,https://172.30.1.21:2379 \
  --cacert=/etc/kubernetes/pki/etcd/ca.crt \
  --cert=/etc/kubernetes/pki/etcd/server.crt \
  --key=/etc/kubernetes/pki/etcd/server.key \
  --name=cl1-k8s-cp-n2 \
  --initial-cluster=cl1-k8s-cp-n3=https://172.30.1.22:2380, cl1-k8s-cp-n1=https://172.30.1.20:2380,cl1-k8s-cp-n2=https://172.30.1.21 :2380 \
  --initial-advertise-peer-urls=https://172.30.1.21:2380

[root@cl1-k8s-cp-n3 ~]# etcdctl snapshot restore backup-dir/snapshot.db --data-dir /var/lib/etcd-restored-19.05.21 \
  --endpoints=https://127.0.0.1:2379,https://172.30.1.22:2379 \
  --cacert=/etc/kubernetes/pki/etcd/ca.crt \
  --cert=/etc/kubernetes/pki/etcd/server.crt \
  --key=/etc/kubernetes/pki/etcd/server.key \
  --name=cl1-k8s-cp-n3 \
  --initial-cluster=cl1-k8s-cp-n3=https://172.30.1.22:2380, cl1-k8s-cp-n1=https://172.30.1.20:2380,cl1-k8s-cp-n2=https://172.30.1.21 :2380 \
  --initial-advertise-peer-urls=https://172.30.1.22:2380
		
#Stop all static pods by removing all manifests:
[root@ALL_MASTER_NODES ~]# rm /etc/kubernetes/manifests/*
# Control plane and API will be completely unavailable!
	
# On all nodes, edit the etcd manifest to point at the restored etcd data
# Backed up private keys and certificates can also be specified here if needed
...
  volumes:
  - hostPath:
      path: /etc/kubernetes/pki/etcd
      type: DirectoryOrCreate
    name: etcd-certs
  - hostPath:
      path: /var/lib/etcd-restored-19.05.21
      type: DirectoryOrCreate
    name: etcd-data
...

# Copy the manifests back to /etc/kubenernetes/manifests/
# Restart the kubelet
[root@ALL_MASTER_NODES ~]# systemctl restart kubelet
