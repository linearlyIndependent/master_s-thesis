#!/bin/bash
CLUSTER=rke
USERS="somehow_get_users"
AGROUP="some_group"
APP=nodered-influx-grafana
PWFILE="user-pws-$AGROUP-$APP-$(date +%s).csv"

for AUSER in $USERS; do
  echo "[DEPLOY_K8S_ENV] $AUSER"
  rancher project create --cluster $CLUSTER "$APP-$AUSER"
  rancher context switch $(rancher projects ls | grep "$APP-$AUSER" | awk '{ print $1 }')
  rancher project add-member-role $AUSER view
  rancher namespace create "$APP-$AUSER"

  echo "[CREATE_SECRETS] $AUSER - $APP"
  PASS=$(openssl rand -base64 32)
  HASHPASS=$(echo $PASS | node-red admin hash-pw | sed 's|Password: ||')
  echo "$AUSER $PASS $HASHPASS" >>$PWFILE

  echo "[DEPLOY_PROJECT] $AUSER - $APP"
  sed "s|__NAMESPACE__|$APP-$AUSER|g;s|__PASSWORD__|$PASS|g;s|__PASSWORD_BCRYPT__ |$HASHPASS|g" > template-nodered-influx-grafana.rke.filled.yaml
  rancher kubectl apply -f template-nodered-influx-grafana.rke.filled.yaml
done
