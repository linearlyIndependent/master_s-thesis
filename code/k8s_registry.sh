#!/bin/bash
# Commands specific to CentOS 7
# Create a TLS certificate with SAN and CN
openssl11 req -x509 -nodes -days 365 -newkey rsa:4096 -keyout /home/service/registry_new.key -out /home/service/registry_new.crt -addext "subjectAltName=DNS:registry.k8s.cs.lan,IP:172.30.1.250"

# Add the certificate to the OS's trusted certificates
# Centos 7
cp registry_new.crt /etc/pki/ca-trust/source/anchors/
update-ca-trust
# Ubuntu 20.04
cp registry_new.crt /usr/share/ca-certificates/extra/
dpkg-reconfigure ca-certificates

# Create the registry's namespace
kubectl create namespace my-namespace

# Create the registry's TLS Secret from the private key and the certificate
kubectl create secret tls registry-tls-secret --key registry_new.key --cert registry_new.crt -n docker-registry

# Create the registry's basic authentication Secret from a htpasswd file
kubectl create secret generic registry-htpasswd-secret --from-file=htpasswd -n docker-registry
