#!/bin/bash
AGROUP=some-group
APP=nodered-influx-grafana
CLUSTER=okd
PWFILE="user-pws-$AGROUP-$APP-$(date +%s).csv"
USERS="$(oc get group $AGROUP -o custom-columns=DATA:users | tail -1 | sed 's/[][]//g')"
echo "user pw bcrypt" >$PWFILE

for AUSER in $USERS; do
  echo "[DEPLOY_K8S_ENV] $AUSER"
  oc process user-env-template -n fcs-templates -p USER=$AUSER -p APP=$APP | oc apply -f -
  
  echo "[CREATE_SECRETS] $AUSER - $APP"
  PASS=$(openssl rand -base64 32)
  HASHPASS=$(echo $PASS | node-red admin hash-pw | sed 's|Password: ||')
  echo "$AUSER $PASS $HASHPASS" >>$PWFILE
  
  echo "[DEPLOY_PROJECT] $AUSER - $APP"
  oc adm policy add-role-to-user edit $AUSER -n "$APP-$AUSER"
  oc process "$APP-template" -n fcs-templates -p USER=$AUSER -p APP=$APP -p ADMIN_PASSWORD=$PASS -p ADMIN_PASSWORD_BCRYPT=$HASHPASS -p INGRESS_BASE_DN=apps.$CLUSTER.cs.lan | oc apply -f - --as=$AUSER
  oc adm policy remove-role-from-user edit $AUSER -n "$APP-$AUSER"
done
