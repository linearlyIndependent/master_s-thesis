#!/bin/bash

# Host a caching registry over HTTPS
docker run -d --restart=always --name registry_new_tls_proxy \
-v "$(pwd)"/certs:/certs -v /mnt/registry:/var/lib/registry
-e  REGISTRY_PROXY_REMOTEURL=https://registry-1.docker.io \
-e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
-e REGISTRY_HTTP_TLS_KEY=/certs/domain.key
-e REGISTRY_PROXY_USERNAME=<docker_username> \
-e REGISTRY_PROXY_PASSWORD=<secret_token> \
-p 443:443   registry:2


# Host a private registry over HTTPS with authentication.
docker run --entrypoint htpasswd httpd:2 \
-Bbn service <password> > auth/htpasswd

docker run -d --restart=always --name registry_new_tls \
-v "$(pwd)"/certs:/certs -v "$(pwd)"/auth:/auth \
-v /mnt/registry:/var/lib/registry -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
-e REGISTRY_HTTP_TLS_KEY=/certs/domain.key -e "REGISTRY_AUTH=htpasswd" \
-e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
-e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
-p 443:443   registry:2
